<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page import="com.octest.dto.OffreCompte"%>
<%@ page import="java.util.ArrayList"%>

<!doctype html>
<html>
<head>
<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link rel="icon" href="img/favicon.png" type="image/png">
<title>My Bank - Modifier offre compte</title>
<!-- Bootstrap CSS -->
<link rel="stylesheet" href="css/bootstrap.css">
<link rel="stylesheet" href="vendors/linericon/style.css">
<link rel="stylesheet" href="css/font-awesome.min.css">
<link rel="stylesheet" href="vendors/owl-carousel/owl.carousel.min.css">
<link rel="stylesheet" href="vendors/lightbox/simpleLightbox.css">
<link rel="stylesheet" href="vendors/nice-select/css/nice-select.css">
<link rel="stylesheet" href="vendors/animate-css/animate.css">
<!-- main css -->
<link rel="stylesheet" href="css/style.css">
<link rel="stylesheet" href="css/responsive.css">
<link rel="stylesheet" href="css/StyleContactInscription.css" />
</head>

<body>
	<%@ include file="Nav.jsp"%>
	<%
		int idOffreCompte = Integer.parseInt(request.getParameter("idOffre"));
		ArrayList<OffreCompte> listeOffreCompte = (ArrayList<OffreCompte>) request.getAttribute("OffreCompte");
	%>
	<section id="formulaire">
		<div class="container">
			<form method="post" action="ModifierOffreCompte" enctype="multipart/form-data">
				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
							<label for="type_compte">Type de compte</label> <input required type="text"
								name="type_compte" class="form-control" id="type_compte" value="<%=listeOffreCompte.get(idOffreCompte).getTypeCompte()%>">
						</div>
					</div>
					<div class="col-md-12">
						<div class="form-group">
							<label for="description_compte">Description de l'offre sur les comptes</label>
							<textarea required id="description_compte" name="description_compte"
								class="form-control"><%=listeOffreCompte.get(idOffreCompte).getDescriptionCompte().replace("<br/>", "")%></textarea>
						</div>
					</div>
					<div class="col-md-12">
						<div class="form-group">
							<label for="photo_compte">Photo pour l'offre : </label>
            				<input type="file" name="photo_compte" id="photo_compte" value="<%="/Users/christopher/eclipse-workspace/ProjetBanqueS8/WebContent/img"+listeOffreCompte.get(idOffreCompte).getPhotoCompte()%>"/>
						</div>
					</div>
					<div class="col-md-12">
						<div class="checkbox">
							<label for="confirmation"><input required type="checkbox"
								name="confirmation" id="confirmation"> Veuillez confirmer
								votre ajout</label>
						</div>
					</div>
					<div class="col-md-12">
						<button type='submit' class='btn btn-primary' name="idActu" value="<%=listeOffreCompte.get(idOffreCompte).getIdOffreCompte()%>">Envoyer</button>
					</div>
				</div>
			</form>
		</div>
	</section>
	<%@ include file="Footer.jsp"%>
</body>

</html>
