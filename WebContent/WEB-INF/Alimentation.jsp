<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<!doctype html>
<html lang="fr">
<head>
<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link rel="icon" href="img/favicon.png" type="image/png">
<title>My Bank - Dépôt d'argent</title>
<!-- Bootstrap CSS -->
<link rel="stylesheet" href="css/bootstrap.css">
<link rel="stylesheet" href="vendors/linericon/style.css">
<link rel="stylesheet" href="css/font-awesome.min.css">
<link rel="stylesheet" href="vendors/owl-carousel/owl.carousel.min.css">
<link rel="stylesheet" href="vendors/lightbox/simpleLightbox.css">
<link rel="stylesheet" href="vendors/nice-select/css/nice-select.css">
<link rel="stylesheet" href="vendors/animate-css/animate.css">
<!-- main css -->
<link rel="stylesheet" href="css/style.css">
<link rel="stylesheet" href="css/responsive.css">
</head>
<body>

	<%@ include file="Nav.jsp"%>
	
<section id="formulaire">
		<div class="container" id="info">
			<div class="row">
				<div class="col-md-12">
					<hr>
					<div class="alert alert-info">
						<p style="color: blue">
							<b style="color: blue">INFOS :</b> Veuillez remplir tous les champs pour alimenter votre compte
						</p>
					</div>
					<hr>
				</div>
			</div>
		</div>
		<div class="container">
			<form method="post" action="Alimentation">
				<div class="row">
					<div class="col-md-12">
						<select required name="menu_compte" id="menu_compte">
							<option value="">----------</option>
							<option value="Compte Courant">Compte Courant</option>
							<option value="Compte Epargne">Compte Epargne</option>
						</select>
					</div>
				</br>-->
					
					<div class="col-md-6">
						<div class="form-group">
							<label for="prenom">Le montant de votre dépôt</label> <input required
								type="text" name="cash" class="form-control" id="prenom">
						</div>
					</div>
					<div class="col-md-12">
						<div class="checkbox">
							<label for="confirmation"><input required type="checkbox"
								name="confirmation" id="confirmation">Veuillez confirmer
								votre demande</label>
						</div>
					</div>
					<div class="col-md-12">
						<button type='submit' class='btn btn-primary'>Envoyer</button>
					</div>
				</div>
			</form>
		</div>
	</section>
	</br>
	</br>
	<%@ include file="Footer.jsp"%>
</body>
</html>