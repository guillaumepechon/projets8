<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>

<head>
<meta charset="utf-8" />
<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link rel="icon" href="img/favicon.png" type="image/png">
<title>My Bank - Virement Interne</title>
<!-- Bootstrap CSS -->
<link rel="stylesheet" href="css/bootstrap.css">
<link rel="stylesheet" href="vendors/linericon/style.css">
<link rel="stylesheet" href="css/font-awesome.min.css">
<link rel="stylesheet" href="vendors/owl-carousel/owl.carousel.min.css">
<link rel="stylesheet" href="vendors/lightbox/simpleLightbox.css">
<link rel="stylesheet" href="vendors/nice-select/css/nice-select.css">
<link rel="stylesheet" href="vendors/animate-css/animate.css">
<!-- main css -->
<link rel="stylesheet" href="css/style.css">
<link rel="stylesheet" href="css/responsive.css">
<link rel="stylesheet" href="css/StyleContactInscription.css" />
</head>

<body>
	<%@ include file="Nav.jsp"%>
	<section id="formulaire">
		<div class="container" id="info">
			<div class="row">
				<div class="col-md-12">
					<hr>
					<div class="alert alert-info">
						<p style="color:blue">
							<b style="color:blue">INFOS :</b> <h3>Vous effectuez un virement vers le Compte Courant</h3></br>Veuillez compléter les différents champs pour effectuer votre virement.</br>
						</p>
					</div>
					<hr>
				</div>
			</div>
		</div>
		<div class="limiter">
			<div class="container-login100">
				<div class="wrap-login100 ">
				     <span class="login100-form-title">
							Virement Interne
					 </span>
			<form method="post" action="InterneEpargne">
				<div class="row">
					
					<div class="col-md-6">
						<div class="form-group">
							<label class= "form-type" for="lieu_naissance"> Montant </label>
							<input type="text" class="form-control" id="lieu_naissance" name="cash" placeholder="Entrez le montant du virement">
						</div>
					</div>
					
					<div class="col-md-6">
						<div class="form-group">
							<label class= "form-type" for="adresse"> Motif du virement</label>
							<input type="text" class="form-control" id="adresse" name="motif" placeholder="Entrez le motif du virement ">
						</div>
					</div>

					
					<div class="col-md-12">
						<button type='submit' class='btn btn-success'>Envoyer</button>
					</div>
					
				</div>
			</form>
			</div>
		</div>
	</div>
	</section>

	<%@ include file="Footer.jsp"%>
	<!--================ End footer Area  =================-->




	<!-- Optional JavaScript -->
	<!-- jQuery first, then Popper.js, then Bootstrap JS -->
	<script src="js/jquery-3.2.1.min.js"></script>
	<script src="js/popper.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/stellar.js"></script>
	<script src="vendors/lightbox/simpleLightbox.min.js"></script>
	<script src="vendors/nice-select/js/jquery.nice-select.min.js"></script>
	<script src="vendors/isotope/imagesloaded.pkgd.min.js"></script>
	<script src="vendors/isotope/isotope-min.js"></script>
	<script src="vendors/owl-carousel/owl.carousel.min.js"></script>
	<script src="js/jquery.ajaxchimp.min.js"></script>
	<script src="vendors/counter-up/jquery.waypoints.min.js"></script>
	<script src="vendors/counter-up/jquery.counterup.js"></script>
	<script src="js/mail-script.js"></script>
	<script src="js/theme.js"></script>
</body>
</html>
