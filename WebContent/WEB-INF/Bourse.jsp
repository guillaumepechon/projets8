<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link rel="icon" href="img/favicon.png" type="image/png">
<title>My Bank - Bourse</title>
<!-- Bootstrap CSS -->
<link rel="stylesheet" href="css/bootstrap.css">
<link rel="stylesheet" href="vendors/linericon/style.css">
<link rel="stylesheet" href="css/font-awesome.min.css">
<link rel="stylesheet" href="vendors/owl-carousel/owl.carousel.min.css">
<link rel="stylesheet" href="vendors/lightbox/simpleLightbox.css">
<link rel="stylesheet" href="vendors/nice-select/css/nice-select.css">
<link rel="stylesheet" href="vendors/animate-css/animate.css">
<!-- main css -->
<link rel="stylesheet" href="css/style.css">
<link rel="stylesheet" href="css/responsive.css">
<link rel="stylesheet" href="css/StyleMenuEspaceClient.css">
</head>
<body>
	<%@ include file="Nav.jsp"%>

	<div class="limiter">
		<div class="container-login100">
			<div class="row">
				<div class="col-11">
					<div class="card"
						style="margin-top: 60px; margin-left: 110px; background-color: grey;">
						<div class="card-body">
							<h2 class="card-title" style="text-align: center; color: white;">Valeur
								des Actions du CAC40</h2>
							<div class="row">
								<c:forEach var="Cac" items="${liste_cac}" varStatus="vs">
									<div class="col-sm-3">
										<div class="card"
											style="margin-top: 20px; text-align: center;">
											<div class="card-body">
												<h5 class="card-title"
													style="text-align: center; color: #222222;">
													<c:out value="${Cac.nom}"></c:out>
												</h5>
												<p class="card-text"
													style="text-align: center; color: #222222;">
													<c:out value="${Cac.dernier} €"></c:out>
												</p>
												<button type="button" class="btn btn-success"
													data-toggle="modal" data-target="#ModalAcheter${vs.index}">Acheter</button>
												<button type="button" class="btn btn-danger">Vendre</button>
											</div>
										</div>
									</div>
									<div class="modal fade" id="ModalAcheter${vs.index}" tabindex="-1"
										role="dialog" aria-labelledby="exampleModalCenterTitle"
										aria-hidden="true">
										<div class="modal-dialog modal-dialog-centered" style="top: 50%; transform: translateY(-50%);"
											role="document">
											<div class="modal-content">
												<div class="modal-header">
													<h5 class="modal-title" id="exampleModalLongTitle">Acheter Actions de <c:out value="${Cac.nom}"></c:out> </h5>
													<button type="button" class="close" data-dismiss="modal"
														aria-label="Close">
														<span aria-hidden="true">&times;</span>
													</button>
												</div>
												<div class="modal-body">
													<c:if test="${empty sessionScope.idClient}"> 
													<p>Veuillez vous connecter pour passer des ordres</p>
													<a href="/ProjetBanqueS8/Connexion" class="btn btn-success">Connexion</a>
													</c:if>
													<c:if test="${empty sessionScope.idTitre && !empty sessionScope.idClient}">
													<p>Veuillez creer un compte titre pour passer des ordres</p>
													<a href="/ProjetBanqueS8/EspaceClient - Creer Compte Titre" class="btn btn-success">Créer Compte Titre</a>
													</c:if>
													<c:if test="${!empty sessionScope.idTitre}">
				                                    <h4>Solde Compte Titre : ${sessionScope.solde_titre} €</h4>
													<form method="get" action="Bourse" name="bourse">
													<div class="form-group">
													<label class="form-type" for="nbre"> Indiquez le nombre d'action de <c:out value="${Cac.nom}"></c:out> que vous souhaitez acheter : </label>
													<input type="number" class="form-control" name="nbre" id="nbre" placeholder="ex: 53" style="width: 20%; display: inline;" min="0">
													<p style="display: inline;"> actions à </p>
													<input type="text" class="form-control" name="prix" id="prix" value="${Cac.dernier}" readonly style="width: 20%; display: inline;">
													<p style="display: inline;"> € </p>
													<p id="montant"></p>
													</div>
													<div class="form-group">
													     <button type="submit" class="btn btn-success"> Passer l'ordre </button>
													</div>
													</form>
													</c:if>
												</div>
												<div class="modal-footer">
													<button type="button" class="btn btn-secondary"
														data-dismiss="modal">Close</button>
													<button type="button" class="btn btn-primary">Save
														changes</button>
												</div>
											</div>
										</div>
									</div>
								</c:forEach>
							</div>

						</div>
					</div>
				</div>
			</div>
		</div>
	</div>


	<script src="js/jquery-3.2.1.min.js"></script>
	<script src="js/popper.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/stellar.js"></script>
	<script src="vendors/lightbox/simpleLightbox.min.js"></script>
	<script src="vendors/nice-select/js/jquery.nice-select.min.js"></script>
	<script src="vendors/isotope/imagesloaded.pkgd.min.js"></script>
	<script src="vendors/isotope/isotope-min.js"></script>
	<script src="vendors/owl-carousel/owl.carousel.min.js"></script>
	<script src="js/jquery.ajaxchimp.min.js"></script>
	<script src="vendors/counter-up/jquery.waypoints.min.js"></script>
	<script src="vendors/counter-up/jquery.counterup.js"></script>
	<script src="js/mail-script.js"></script>
	<script src="js/theme.js"></script>
	<script src="js/bourse.js"></script>

</body>
</html>