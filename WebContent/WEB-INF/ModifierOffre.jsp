<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page import="com.octest.dto.Offre"%>
<%@ page import="java.util.ArrayList"%>

<!doctype html>
<html>
<head>
<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link rel="icon" href="img/favicon.png" type="image/png">
<title>My Bank - Modifier offre de service</title>
<!-- Bootstrap CSS -->
<link rel="stylesheet" href="css/bootstrap.css">
<link rel="stylesheet" href="vendors/linericon/style.css">
<link rel="stylesheet" href="css/font-awesome.min.css">
<link rel="stylesheet" href="vendors/owl-carousel/owl.carousel.min.css">
<link rel="stylesheet" href="vendors/lightbox/simpleLightbox.css">
<link rel="stylesheet" href="vendors/nice-select/css/nice-select.css">
<link rel="stylesheet" href="vendors/animate-css/animate.css">
<!-- main css -->
<link rel="stylesheet" href="css/style.css">
<link rel="stylesheet" href="css/responsive.css">
<link rel="stylesheet" href="css/StyleContactInscription.css" />
</head>

<body>
	<%@ include file="Nav.jsp"%>
	<%
		int idOffre = Integer.parseInt(request.getParameter("idOffre"));
		ArrayList<Offre> listeOffre = (ArrayList<Offre>) request.getAttribute("offres");
	%>
	<section id="formulaire">
		<div class="container">
			<form method="post" action="ModifierOffre">
				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
							<label for="description">Description de l'offre</label>
							<textarea required id="description" name="description"
								class="form-control"><%=listeOffre.get(idOffre).getOff_description()%></textarea>
						</div>
					</div>
					<div class="col-md-12">
						<div class="form-group">
							<label for="tarif">Tarif de l'offre</label>
							<textarea required id="tarif" name="tarif"
								class="form-control"><%=listeOffre.get(idOffre).getOff_tarif()%></textarea>
						</div>
					</div>
					<div class="col-md-12">
						<div class="checkbox">
							<label for="confirmation"><input required type="checkbox"
								name="confirmation" id="confirmation"> Veuillez confirmer
								votre ajout</label>
						</div>
					</div>
					<div class="col-md-12">
						<button type='submit' class='btn btn-primary' name="idOffre" value="<%=listeOffre.get(idOffre).getIdOffre()%>">Envoyer</button>
					</div>
				</div>
			</form>
		</div>
	</section>
	<%@ include file="Footer.jsp"%>
</body>

</html>