<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="com.octest.dto.Actualite"%>
<%@ page import="java.util.ArrayList"%>

<!doctype html>
<html lang="fr">
<head>
<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link rel="icon" href="img/favicon.png" type="image/png">
<title>My Bank - Actualités détaillées</title>
<!-- Bootstrap CSS -->
<link rel="stylesheet" href="css/bootstrap.css">
<link rel="stylesheet" href="vendors/linericon/style.css">
<link rel="stylesheet" href="css/font-awesome.min.css">
<link rel="stylesheet" href="vendors/owl-carousel/owl.carousel.min.css">
<link rel="stylesheet" href="vendors/lightbox/simpleLightbox.css">
<link rel="stylesheet" href="vendors/nice-select/css/nice-select.css">
<link rel="stylesheet" href="vendors/animate-css/animate.css">
<!-- main css -->
<link rel="stylesheet" href="css/style.css">
<link rel="stylesheet" href="css/responsive.css">
<link rel="stylesheet" href="css/StyleContactInscription.css" />

<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
</head>
<body>

	<!--================Header Menu Area =================-->
	<%@ include file="Nav.jsp"%>
	<!--================Header Menu Area =================-->

	<!--================Home Banner Area =================-->
	<section class="banner_area">
		<div class="banner_inner d-flex align-items-center">
			<div class="container">
				<div class="banner_content text-center">
					<div class="page_link">
						<a href="/ProjetBanqueS8/">Accueil</a>
						<a href="/ProjetBanqueS8/ActualiteBanque">Actualités</a>
					</div>
					<h2>Actualités</h2>
				</div>
			</div>
		</div>
		
		
		<div class="container">
			<div class="main_title" Style="margin-top:20px;">
				<h2>Suivez l'actualité de la Team Bank</h2>
				<p>L'actualité en temps réel de notre banque</p>
			</div>
			<div class="row feature_inner">
				<%
					int actualite = Integer.parseInt(request.getParameter("actualite"));
					ArrayList<Actualite> listeActualite = (ArrayList<Actualite>) request.getAttribute("actualites");
					if (listeActualite != null && !listeActualite.isEmpty()) {
						out.write("<div class='row'>");
						out.write("<img src='img/"+ listeActualite.get(actualite).getAct_photo() + "' alt='Photo actualite' Style='height:75%; width:50%; margin-left:auto; margin-right:auto;'>");
						out.write("</div>");
						out.write("<div class='row' Style='margin-left:auto; margin-right:auto;'>");
						out.write("<h4><br>" + listeActualite.get(actualite).getAct_title() + "</br></h4>");
						out.write("</div>");
						out.write("<div class='row' Style='margin-left:auto; margin-right:auto; margin-top:20px; margin-bottom:20px;'>");
						out.write("<p>" + listeActualite.get(actualite).getAct_contenu_det() + "</p>");
						out.write("</div>");
					}
				%>
			</div>
		</div>
	</section>
	<%@ include file="Footer.jsp"%>
</body>	
		<!--================End Home Banner Area =================-->
	
</html>