<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport"
			content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<link rel="icon" href="img/favicon.png" type="image/png">
		<title>My Bank - Mon Espace Client</title>
		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="css/bootstrap.css">
		<link rel="stylesheet" href="vendors/linericon/style.css">
		<link rel="stylesheet" href="css/font-awesome.min.css">
		<link rel="stylesheet" href="vendors/owl-carousel/owl.carousel.min.css">
		<link rel="stylesheet" href="vendors/lightbox/simpleLightbox.css">
		<link rel="stylesheet" href="vendors/nice-select/css/nice-select.css">
		<link rel="stylesheet" href="vendors/animate-css/animate.css">
		<!-- main css -->
		<link rel="stylesheet" href="css/style.css">
		<link rel="stylesheet" href="css/responsive.css">
		<link rel="stylesheet" href="css/StyleMenuEspaceClient.css">
	</head>
	<body>
	<%@ include file="Nav.jsp"%>
	
	<div class="limiter">
	<div class="container-login100">
	<div class="row">
	  <div class="col-4">
		<div class="list-group">
		  <a href="/ProjetBanqueS8/EspaceClient - Profil" class="list-group-item active">Mon profil</a>
		  <a href="#" class="list-group-item">Mon Compte</a>
		  <a href="#" class="list-group-item">Morbi leo risus</a>
		  <a href="#" class="list-group-item">Porta ac consectetur ac</a>
		  <a href="#" class="list-group-item">Vestibulum at eros</a>
		</div>
		<div class="wrap-login100 ">
				     <span class="login100-form-title">
							Modifier mes informations Personnelles 
					 </span>
			<form method="post" action="EspaceClient_ModifierProfil">
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label class= "form-type" for="nom">Nom</label> 
							<input required type="text"	name="nom" class="form-control" id="nom" placeholder="Entrez votre Nom">
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label class= "form-type" for="prenom">Prénom</label> 
							<input required type="text" name="prenom" class="form-control" id="prenom" placeholder="Entrez votre Prénom">
						</div>
					</div>
					<div class="col-md-12">
						<div class="form-group">
							<label class= "form-type" for="mail">Email</label> 
							<input required	type="email" class="form-control" name="mail" id="mail" placeholder="Entrez votre Email">
						    <small class= "form-small" > un Email de validation vous sera envoyé </small>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label class= "form-type" for="mot_de_passe"> Mot de Passe</label>
							<input required type="password" class="form-control" id="mot_de_passe" name="mot_de_passe" placeholder="Entrez votre Mot de Passe">
						</div>
					</div>
					
					<div class="col-md-6">
						<div class="form-group">
							<label class= "form-type" for="conf_mdp">Confirmer Mot de Passe</label>
							<input required type="password" class="form-control" id="conf_mdp" name="conf_mdp" placeholder="Confirmez votre Mot de Passe">
						</div>
					</div>
					
					<div class="col-md-6">
						<div class="form-group">
							<label class= "form-type" for="date_naissance"> Date de Naissance </label>
							<input type="date" class="form-control" id="date_naissance" name="date_naissance">
						</div>
					</div>
					
					<div class="col-md-6">
						<div class="form-group">
							<label class= "form-type" for="lieu_naissance"> Lieu de Naissance </label>
							<input type="text" class="form-control" id="lieu_naissance" name="lieu_naissance" placeholder="Entrez votre lieu de naissance">
						</div>
					</div>
					
					<div class="col-md-6">
						<div class="form-group">
							<label class= "form-type" for="adresse"> Adresse (n° Rue, Voie) </label>
							<input type="text" class="form-control" id="adresse" name="adresse" placeholder="Entrez votre adresse ">
						</div>
					</div>
					
					<div class="col-md-6">
						<div class="form-group">
							<label class= "form-type" for="ville"> Ville </label>
							<input type="text" class="form-control" id="ville" name="ville" placeholder="Entrez votre ville actuelle ">
						</div>
					</div>
					
					<div class="col-md-12">
						<button type='submit' class='btn btn-success'>Modifier</button>
						<a class="btn btn-warning" type="button" href="/ProjetBanqueS8/EspaceClient - Profil">Annuler</a>
					</div>
					
				</div>
			</form>
			</div>
	  </div>
	</div>
	</div>
	</div>  

	<%@ include file="Footer.jsp"%>
	<!--================ End footer Area  =================-->

	<!-- Optional JavaScript -->
	<!-- jQuery first, then Popper.js, then Bootstrap JS -->
	<script src="js/jquery-3.2.1.min.js"></script>
	<script src="js/popper.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/stellar.js"></script>
	<script src="vendors/lightbox/simpleLightbox.min.js"></script>
	<script src="vendors/nice-select/js/jquery.nice-select.min.js"></script>
	<script src="vendors/isotope/imagesloaded.pkgd.min.js"></script>
	<script src="vendors/isotope/isotope-min.js"></script>
	<script src="vendors/owl-carousel/owl.carousel.min.js"></script>
	<script src="js/jquery.ajaxchimp.min.js"></script>
	<script src="vendors/counter-up/jquery.waypoints.min.js"></script>
	<script src="vendors/counter-up/jquery.counterup.js"></script>
	<script src="js/mail-script.js"></script>
	<script src="js/theme.js"></script>
	</body>
</html>