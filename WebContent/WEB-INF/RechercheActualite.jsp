<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="com.octest.dto.Actualite"%>
<%@ page import="java.util.ArrayList"%>

<!doctype html>
<html lang="fr">
<head>
<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link rel="icon" href="img/favicon.png" type="image/png">
<title>My Bank - Actualités</title>
<!-- Bootstrap CSS -->
<link rel="stylesheet" href="css/bootstrap.css">
<link rel="stylesheet" href="vendors/linericon/style.css">
<link rel="stylesheet" href="css/font-awesome.min.css">
<link rel="stylesheet" href="vendors/owl-carousel/owl.carousel.min.css">
<link rel="stylesheet" href="vendors/lightbox/simpleLightbox.css">
<link rel="stylesheet" href="vendors/nice-select/css/nice-select.css">
<link rel="stylesheet" href="vendors/animate-css/animate.css">
<!-- main css -->
<link rel="stylesheet" href="css/style.css">
<link rel="stylesheet" href="css/responsive.css">
<link rel="stylesheet" href="css/StyleContactInscription.css" />

<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
</head>
<body>

	<!--================Header Menu Area =================-->
	<%@ include file="Nav.jsp"%>
	<!--================Header Menu Area =================-->

	<!--================Home Banner Area =================-->
	<section class="banner_area">
		<div class="banner_inner d-flex align-items-center">
			<div class="container">
				<div class="banner_content text-center">
					<div class="page_link">
						<a href="/ProjetBanqueS8/">Accueil</a>
						<a href="/ProjetBanqueS8/ActualiteBanque">Actualités</a>
					</div>
					<h2>Resultat de la recherche dans les actualités</h2>
				</div>
			</div>
		</div>

		<!--================End Home Banner Area =================-->
		<div class="container">
			<div id="carouselExampleIndicators" class="carousel slide"
				data-ride="carousel">
				<ol class="carousel-indicators">
					<li data-target="#carouselExampleIndicators" data-slide-to="0"
						class="active"></li>
					<li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
					<li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
				</ol>
				<div class="carousel-inner">
					<div class="carousel-item active">
						<img class="d-block w-100" src="img/actu1.jpg" alt="First slide">
					</div>
					<div class="carousel-item">
						<img class="d-block w-100" src="img/actu2.jpeg" alt="Second slide">
					</div>
					<div class="carousel-item">
						<img class="d-block w-100" src="img/actu3.jpg" alt="Third slide">
					</div>
				</div>
				<a class="carousel-control-prev" href="#carouselExampleIndicators"
					role="button" data-slide="prev"> <span
					class="carousel-control-prev-icon" aria-hidden="true"></span> <span
					class="sr-only">Previous</span>
				</a> <a class="carousel-control-next" href="#carouselExampleIndicators"
					role="button" data-slide="next"> <span
					class="carousel-control-next-icon" aria-hidden="true"></span> <span
					class="sr-only">Next</span>
				</a>
			</div>
		</div>
	</section>
	<!--================About Area =================-->
	<!-- <section class="about_area p_120">
        	<div class="container">
        		<div class="row about_inner">
        			<div class="col-lg-6">
        				<div class="about_img">
        					<img class="img-fluid" src="img/about-img.jpg" alt="">
        				</div>
        			</div>
        			<div class="col-lg-6">
        				<div class="about_right_text">
        					<h4>Who we are <br />to Serve the nation</h4>
        					<p>inappropriate behavior is often laughed off as âboys will be boys,â women face higher conduct standards especially in the workplace. Thatâs why itâs crucial that, as women, our behavior on the job is beyond reproach.</p>
        					<p>inappropriate behavior is often laughed off as âboys will be boys,â women face higher conduct standards especially in the workplace. Thatâs why itâs crucial that, as women, our behavior on the job is beyond reproach. inappropriate behavior is often laughed off as âboys will be boys,â women face higher conduct standards especially in the workplace. Thatâs why itâs crucial that, as women, our behavior on the job is beyond reproach.</p>
        				</div>
        			</div>
        		</div>
        	</div>
        </section> -->
	<!--================End About Area =================-->

	<!--================Feature Area =================-->
	<section class="feature_area p_120">
		<div class="container">
			<div class="main_title">
				<h2>Suivez l'actualité de la Team Bank</h2>
				<p>L'actualité en temps réel de notre banque</p>
			</div>
			<div class="row feature_inner">
				<%
					ArrayList<Actualite> listeActualite = (ArrayList<Actualite>) request.getAttribute("actualites");
					if (listeActualite != null && !listeActualite.isEmpty()) {
						for (int i = 0; i < listeActualite.size(); i++) {
							out.write("<div class='col-lg-3 col-sm-6' id='card'>");
							out.write("<div class='feature_item' id='hauteur_texte'>");
							out.write("<img src='img/"+ listeActualite.get(i).getAct_photo() + "' alt='Photo actualite' style='max-width:200px'>");
							out.write("<h4><br>" + listeActualite.get(i).getAct_title() + "</br></h4>");
							out.write("<p>" + listeActualite.get(i).getAct_contenu() + "</p>");
							out.write("<a class='nav-link' type='button'  href='/ProjetBanqueS8/ActualiteDetailleeBanque?actualite="+i+"' style='margin-top:10px'>Voir l'article</a>");
							out.write("</div>");
							out.write("</div>");
						}
					}
				%>
			</div>
		</div>
	</section>
	<!--================End Feature Area =================-->


	<!--================Team Area =================-->
	<!-- <section class="team_area p_120">
        	<div class="container">
        		<div class="main_title">
        			<h2>Energetic Team Members</h2>
        			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt</p>
        		</div>
        		<div class="row team_inner">
        			<div class="col-lg-3 col-sm-6">
        				<div class="team_item">
        					<div class="team_img">
        						<img class="img-fluid" src="img/team/team-1.jpg" alt="">
        						<div class="hover">
        							<p>This article is floated online with an aim to help you find the best dvd printing solution. Dvd printing is an</p>
        							<a href="#"><i class="fa fa-facebook"></i></a>
        							<a href="#"><i class="fa fa-twitter"></i></a>
        							<a href="#"><i class="fa fa-linkedin"></i></a>
        						</div>
        					</div>
        					<div class="team_name">
        						<h4>Ethel Davis</h4>
        						<p>Managing Director (Sales)</p>
        					</div>
        				</div>
        			</div>
        			<div class="col-lg-3 col-sm-6">
        				<div class="team_item">
        					<div class="team_img">
        						<img class="img-fluid" src="img/team/team-2.jpg" alt="">
        						<div class="hover">
        							<p>This article is floated online with an aim to help you find the best dvd printing solution. Dvd printing is an</p>
        							<a href="#"><i class="fa fa-facebook"></i></a>
        							<a href="#"><i class="fa fa-twitter"></i></a>
        							<a href="#"><i class="fa fa-linkedin"></i></a>
        						</div>
        					</div>
        					<div class="team_name">
        						<h4>Rodney Cooper</h4>
        						<p>Creative Art Director (Project)</p>
        					</div>
        				</div>
        			</div>
        			<div class="col-lg-3 col-sm-6">
        				<div class="team_item">
        					<div class="team_img">
        						<img class="img-fluid" src="img/team/team-3.jpg" alt="">
        						<div class="hover">
        							<p>This article is floated online with an aim to help you find the best dvd printing solution. Dvd printing is an</p>
        							<a href="#"><i class="fa fa-facebook"></i></a>
        							<a href="#"><i class="fa fa-twitter"></i></a>
        							<a href="#"><i class="fa fa-linkedin"></i></a>
        						</div>
        					</div>
        					<div class="team_name">
        						<h4>Dora Walker</h4>
        						<p>Senior Core Developer</p>
        					</div>
        				</div>
        			</div>
        			<div class="col-lg-3 col-sm-6">
        				<div class="team_item">
        					<div class="team_img">
        						<img class="img-fluid" src="img/team/team-4.jpg" alt="">
        						<div class="hover">
        							<p>This article is floated online with an aim to help you find the best dvd printing solution. Dvd printing is an</p>
        							<a href="#"><i class="fa fa-facebook"></i></a>
        							<a href="#"><i class="fa fa-twitter"></i></a>
        							<a href="#"><i class="fa fa-linkedin"></i></a>
        						</div>
        					</div>
        					<div class="team_name">
        						<h4>Lena Keller</h4>
        						<p>Creative Content Developer</p>
        					</div>
        				</div>
        			</div>
        		</div>
        	</div>
        </section> -->
	<!--================End Team Area =================-->

	<!--================Text Members Area =================-->
	<!--  <section class="text_members_area p_120">
        	<div class="container">
        		<div class="main_title">
        			<h2>Testimonials</h2>
        			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt</p>
        		</div>
        		<div class="member_slider owl-carousel">
        			<div class="item">
        				<div class="member_item">
        					<p>Accessories Here you can find the best computer accessory for your laptop, monitor, printer, scanner, speaker, projector, hardware and more. laptop accessory compaq laptop battery compaq</p>
        					<h4>Mark Alviro Wiens</h4>
        					<h5>CEO at Google</h5>
        				</div>
        			</div>
        			<div class="item">
        				<div class="member_item">
        					<p>Accessories Here you can find the best computer accessory for your laptop, monitor, printer, scanner, speaker, projector, hardware and more. laptop accessory compaq laptop battery compaq</p>
        					<h4>Mark Alviro Wiens</h4>
        					<h5>CEO at Google</h5>
        				</div>
        			</div>
        			<div class="item">
        				<div class="member_item">
        					<p>Accessories Here you can find the best computer accessory for your laptop, monitor, printer, scanner, speaker, projector, hardware and more. laptop accessory compaq laptop battery compaq</p>
        					<h4>Mark Alviro Wiens</h4>
        					<h5>CEO at Google</h5>
        				</div>
        			</div>
        			<div class="item">
        				<div class="member_item">
        					<p>Accessories Here you can find the best computer accessory for your laptop, monitor, printer, scanner, speaker, projector, hardware and more. laptop accessory compaq laptop battery compaq</p>
        					<h4>Mark Alviro Wiens</h4>
        					<h5>CEO at Google</h5>
        				</div>
        			</div>
        			<div class="item">
        				<div class="member_item">
        					<p>Accessories Here you can find the best computer accessory for your laptop, monitor, printer, scanner, speaker, projector, hardware and more. laptop accessory compaq laptop battery compaq</p>
        					<h4>Mark Alviro Wiens</h4>
        					<h5>CEO at Google</h5>
        				</div>
        			</div>
        			<div class="item">
        				<div class="member_item">
        					<p>Accessories Here you can find the best computer accessory for your laptop, monitor, printer, scanner, speaker, projector, hardware and more. laptop accessory compaq laptop battery compaq</p>
        					<h4>Mark Alviro Wiens</h4>
        					<h5>CEO at Google</h5>
        				</div>
        			</div>
        		</div>
        	</div>
        </section> -->
	<!--================End Text Members Area =================-->

	<!--================ start footer Area  =================-->
	<%@ include file="Footer.jsp"%>
	<!--================ End footer Area  =================-->




	<!-- Optional JavaScript -->
	<!-- jQuery first, then Popper.js, then Bootstrap JS -->
	<script src="js/jquery-3.2.1.min.js"></script>
	<script src="js/popper.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/stellar.js"></script>
	<script src="vendors/lightbox/simpleLightbox.min.js"></script>
	<script src="vendors/nice-select/js/jquery.nice-select.min.js"></script>
	<script src="vendors/isotope/imagesloaded.pkgd.min.js"></script>
	<script src="vendors/isotope/isotope-min.js"></script>
	<script src="vendors/owl-carousel/owl.carousel.min.js"></script>
	<script src="js/jquery.ajaxchimp.min.js"></script>
	<script src="vendors/counter-up/jquery.waypoints.min.js"></script>
	<script src="vendors/counter-up/jquery.counterup.js"></script>
	<script src="js/mail-script.js"></script>
	<script src="js/theme.js"></script>
</body>
</html>