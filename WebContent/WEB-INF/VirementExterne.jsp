<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!doctype html>
<html>
<head>
<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link rel="icon" href="img/favicon.png" type="image/png">
<title>My Bank - Virement externe</title>
<!-- Bootstrap CSS -->
<link rel="stylesheet" href="css/bootstrap.css">
<link rel="stylesheet" href="vendors/linericon/style.css">
<link rel="stylesheet" href="css/font-awesome.min.css">
<link rel="stylesheet" href="vendors/owl-carousel/owl.carousel.min.css">
<link rel="stylesheet" href="vendors/lightbox/simpleLightbox.css">
<link rel="stylesheet" href="vendors/nice-select/css/nice-select.css">
<link rel="stylesheet" href="vendors/animate-css/animate.css">
<!-- main css -->
<link rel="stylesheet" href="css/style.css">
<link rel="stylesheet" href="css/responsive.css">
<link rel="stylesheet" href="css/StyleContactInscription.css" />
</head>

<body>
	<%@ include file="Nav.jsp"%>
	<section id="formulaire">
		<div class="container">
			<form method="post" action="VirementExterne">
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label for="iban">Iban du compte recepteur</label>
							<input required id="iban" name="iban"
								class="form-control">
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label for="montant">Montant du virement</label>
							<input required id="montant" name="montant"
								class="form-control">
						</div>
					</div>
					<div class="col-md-12">
						<div class="form-group">
							<label for="motif">Motif du virement</label>
							<input required id="motif" name="motif"
								class="form-control">
						</div>
					</div>
					<div class="col-md-12">
						<div class="checkbox">
							<label for="confirmation"><input required type="checkbox"
								name="confirmation" id="confirmation"> Veuillez confirmer
								votre virement</label>
						</div>
					</div>
					<div class="col-md-12">
						<button type='submit' class='btn btn-primary'>Valider</button>
					</div>
				</div>
			</form>
		</div>
	</section>
	<%@ include file="Footer.jsp"%>
</body>

</html>