<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link rel="icon" href="img/favicon.png" type="image/png">
<title>Espace Client - Alimentation</title>
<!-- Bootstrap CSS -->
<link rel="stylesheet" href="css/bootstrap.css">
<link rel="stylesheet" href="vendors/linericon/style.css">
<link rel="stylesheet" href="css/font-awesome.min.css">
<link rel="stylesheet" href="vendors/owl-carousel/owl.carousel.min.css">
<link rel="stylesheet" href="vendors/lightbox/simpleLightbox.css">
<link rel="stylesheet" href="vendors/nice-select/css/nice-select.css">
<link rel="stylesheet" href="vendors/animate-css/animate.css">
<!-- main css -->
<link rel="stylesheet" href="css/style.css">
<link rel="stylesheet" href="css/responsive.css">
<link rel="stylesheet" href="css/StyleMenuEspaceClient.css">
</head>
<body>
	<%@ include file="Nav.jsp"%>
	<div class="limiter">
		<div class="container-login100">
			<div class="row">
				<div class="col-4">
					<div class="list-group">
						<a href="/ProjetBanqueS8/EspaceClient - Profil"
							class="list-group-item">Mon profil</a> <a
							href="/ProjetBanqueS8/EspaceClient - Solde"
							class="list-group-item">Solde Compte</a> <a href="#"
							class="list-group-item">Virements</a> <a
							href="/ProjetBanqueS8/EspaceClient - Alimentation"
							class="list-group-item active">Alimentation</a>
					</div>
				</div>

				<div class="col-6">
					<div class="card" style="margin-top: 20px;">
						<div class="card-body" style="text-align: center;">
							<form method="POST" action="EspaceClient - Alimentation">
								<div class="form-group">
									<label class="form-type" for="montant"> Montant </label> 
									<input type="text" class="form-control" name="montant" id="montant" placeholder="Entrez le montant de l'alimentation">
								</div>
								<div class="form-group">
									<label class="form-type" for="mode"> Choisissez le mode d'alimentation : </label> 
									<select class="form-control sel" name="mode">
									   <option value="CB">Carte Bancaire</option>
									   <option value="Cheque">Chèque</option>
									   <option value="Depot_espece">Dêpot d'espèces</option>
									</select>
								</div>
								<div class="form-group">
								    <input type="submit" name ="submit" class="btn btn-success" value="Valider">
								</div>
								<%
								  if(request.getAttribute("solde") != null){
								%>
								<div class="form-group">
								    <label class="form-type"> ${sessionScope.solde}</label>
								</div>
								<%
								  }
								%>
								<%
								  if(request.getAttribute("msg") != null){
								%>
								<div class="form-group">
								    <a class="form-type" id="msg"></a>
								</div>
								<%
								  }
								%>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

</body>
</html>