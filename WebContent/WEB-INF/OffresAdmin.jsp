<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="com.octest.dto.Offre"%>
<%@ page import="com.octest.dto.OffreCarte"%>
<%@ page import="com.octest.dto.OffreCompte"%>
<%@ page import="java.util.ArrayList"%>

<!DOCTYPE html>
<html>
<head>
<%@ include file="Lien.jsp"%>
<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link rel="icon" href="img/favicon.png" type="image/png">
<title>My Bank - Offres Administrateur</title>
<!-- Bootstrap CSS -->
<link rel="stylesheet" href="css/bootstrap.css">
<link rel="stylesheet" href="vendors/linericon/style.css">
<link rel="stylesheet" href="css/font-awesome.min.css">
<link rel="stylesheet" href="vendors/owl-carousel/owl.carousel.min.css">
<link rel="stylesheet" href="vendors/lightbox/simpleLightbox.css">
<link rel="stylesheet" href="vendors/nice-select/css/nice-select.css">
<link rel="stylesheet" href="vendors/animate-css/animate.css">
<!-- main css -->
<link rel="stylesheet" href="css/style.css">
<link rel="stylesheet" href="css/responsive.css">
<link rel="stylesheet" href="css/StyleContactInscription.css" />
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
</head>

<body>
	<%@ include file="Nav.jsp"%>
	<section class="feature_area p_120">
		<div class="container">
			<div class="main_title">
				<h2>Venez découvrir nos offres</h2>
				<p>Elu banque en ligne de l'année 2018</p>
			</div>
			<div class="container" style="text-align:center">
				<a class="org_btn" href="/ProjetBanqueS8/AjoutOffreCarte"
					style="display: inline-block; margin: auto; margin-bottom: 40px">Ajouter une offre carte bancaire</a>
				<a class="org_btn" href="/ProjetBanqueS8/AjoutOffreCompte"
					style="display: inline-block; margin: auto; margin-bottom: 40px">Ajouter une offre compte bancaire</a>
				<a class="org_btn" href="/ProjetBanqueS8/AjoutOffre"
					style="display: inline-block; margin: auto; margin-bottom: 40px">Ajouter une offre de service</a>
			</div>
			<div class="row feature_inner">
				<%
					ArrayList<OffreCarte> listeOffreCarte = (ArrayList<OffreCarte>) request.getAttribute("offrescartes");
					if (listeOffreCarte != null && !listeOffreCarte.isEmpty()) {
						for (int i = 0; i < listeOffreCarte.size(); i++) {
							out.write("<div class='col-lg-4 col-sm-6' id='card' style='margin-bottom : 20px'>");
							out.write("<div class='feature_item' id='hauteur_texte' style='text-align:center'>");
							out.write("<img src='img/" + listeOffreCarte.get(i).getPhotoCarte()
									+ "' alt='Photo offre carte' style='max-width:200px'>");
							out.write("<h4><br>" + listeOffreCarte.get(i).getTypeCarte() + "</br></h4>");
							out.write("<p>" + listeOffreCarte.get(i).getDescriptionCarte() + "</p>");
							out.write("<a class='org_btn' href='/ProjetBanqueS8/ModifierOffreCarte?idOffre="
									+ i + "' style='display: block; margin: auto; margin-top:10px'>Modifier l'offre</a>");
							out.write("<form action='OffreAdmin' method='post'>");
							out.write("<td><button type='submit' class='org_btn' name='idOffreCarte' value='"+listeOffreCarte.get(i).getIdOffreCarte()+"'>Supprimer</button><td>");
							out.write("</form>");
							out.write("</div>");
							out.write("</div>");
						}
					}
				%>
				<a class="org_btn" href="/ProjetBanqueS8/InscriptionBanque"
					style="display: block; margin: auto; margin-bottom: 40px">Ouvrir
					un compte bancaire</a>
			</div>
		</div>
		<div class="container">
			<div class="main_title">
				<h2>Livret d'épargne en ligne par MyBank</h2>
			</div>
			<div class="row feature_inner">
				<%
					ArrayList<OffreCompte> listeOffreCompte = (ArrayList<OffreCompte>) request.getAttribute("offrescomptes");
					if (listeOffreCompte != null && !listeOffreCompte.isEmpty()) {
						for (int i = 0; i < listeOffreCompte.size(); i++) {
							out.write("<div class='col-lg-12 col-sm-12' id='card'>");
							out.write("<div class='feature_item' id='hauteur_texte' style='text-align:center'>");
							out.write("<img src='img/" + listeOffreCompte.get(i).getPhotoCompte()
									+ "' alt='Photo offre compte' style='max-width:300px'>");
							out.write("<h4><br>" + listeOffreCompte.get(i).getTypeCompte() + "</br></h4>");
							out.write("<p>" + listeOffreCompte.get(i).getDescriptionCompte() + "</p>");
							out.write("<a class='org_btn' href='/ProjetBanqueS8/ModifierOffreCompte?idOffre="
									+ i + "' style='display: block; margin: auto; margin-top:10px'>Modifier l'offre</a>");
							out.write("<form action='OffreAdmin' method='post'>");
							out.write("<td><button type='submit' class='org_btn' name='idOffreCompte' value='"+listeOffreCompte.get(i).getIdOffreCompte()+"'>Supprimer</button><td>");
							out.write("</form>");
							out.write("</div>");
							out.write("</div>");
						}
					}
				%>
				<a class="org_btn" href="/ProjetBanqueS8/InscriptionBanque"
					style="display: block; margin: auto; margin-bottom: 40px">Ouvrir
					un compte bancaire</a>
			</div>
		</div>
		<div class="container">
			<div class="main_title">
				<h2>Liste des services au sein de MyBank</h2>
			</div>
		</div>
		<table class="table table-striped">
			<thead>
				<tr>
					<th scope="col">#</th>
					<th scope="col">Liste des services</th>
					<th scope="col">Prix en euros</th>
					<th scope="col">Modifier</th>
					<th scope="col">Supprimer</th>
				</tr>
			</thead>
			<tbody>
				<%
					ArrayList<Offre> listeOffre = (ArrayList<Offre>) request.getAttribute("offres");
					if (listeOffre != null && !listeOffre.isEmpty()) {
						for (int i = 0; i < listeOffre.size(); i++) {
							out.write("<tr>");
							out.write("<th scope='row'>" + (i + 1) + "</th>");
							out.write("<td>" + listeOffre.get(i).getOff_description() + "</td>");
							out.write("<td>" + listeOffre.get(i).getOff_tarif() + "</td>");
							out.write("<td><a class='org_btn' href='/ProjetBanqueS8/ModifierOffre?idOffre="
									+ i + "' style='display: block; margin: auto; margin-top:10px'>Modifier</a></td>");
							out.write("<form action='OffreAdmin' method='post'>");
							out.write("<td><button type='submit' class='org_btn' name='idOffre' value='"+listeOffre.get(i).getIdOffre()+"'>Supprimer</button><td>");
							out.write("</form>");
						}
					}
				%>
			</tbody>
		</table>

	</section>
	<%@ include file="Footer.jsp"%>
</body>

</html>
