<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!doctype html>
<html>
<head>
<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link rel="icon" href="img/favicon.png" type="image/png">
<title>My Bank - Ajout Offre Compte</title>
<!-- Bootstrap CSS -->
<link rel="stylesheet" href="css/bootstrap.css">
<link rel="stylesheet" href="vendors/linericon/style.css">
<link rel="stylesheet" href="css/font-awesome.min.css">
<link rel="stylesheet" href="vendors/owl-carousel/owl.carousel.min.css">
<link rel="stylesheet" href="vendors/lightbox/simpleLightbox.css">
<link rel="stylesheet" href="vendors/nice-select/css/nice-select.css">
<link rel="stylesheet" href="vendors/animate-css/animate.css">
<!-- main css -->
<link rel="stylesheet" href="css/style.css">
<link rel="stylesheet" href="css/responsive.css">
<link rel="stylesheet" href="css/StyleContactInscription.css" />
</head>

<body>
	<%@ include file="Nav.jsp"%>
	<section id="formulaire">
		<div class="container">
			<form method="post" action="AjoutOffreCompte" enctype="multipart/form-data">
				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
							<label for="type">Type de compte</label> <input required type="text"
								name="type" class="form-control" id="type">
						</div>
					</div>
					<div class="col-md-12">
						<div class="form-group">
							<label for="description">Description du compte</label>
							<textarea required id="description" name="description"
								class="form-control"></textarea>
						</div>
					</div>
					<div class="col-md-12">
						<div class="form-group">
							<label for="fichier">Photo pour le compte : </label>
            				<input type="file" name="fichier" id="fichier" />
						</div>
					</div>
					<div class="col-md-12">
						<div class="checkbox">
							<label for="confirmation"><input required type="checkbox"
								name="confirmation" id="confirmation"> Veuillez confirmer
								votre ajout</label>
						</div>
					</div>
					<div class="col-md-12">
						<button type='submit' class='btn btn-primary'>Envoyer</button>
					</div>
				</div>
			</form>
		</div>
	</section>
	<%@ include file="Footer.jsp"%>
</body>

</html>