<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<header class="header_area">
	<div class="main_menu">
		<nav class="navbar navbar-expand-lg navbar-light">
			<div class="container box_1620">
				<!-- Brand and toggle get grouped for better mobile display -->
				<a class="navbar-brand logo_h" href="/ProjetBanqueS8/"><img
					src="img/logo1.png" alt=""></a>
				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
					<span class="icon-bar"></span> 
					<span class="icon-bar"></span> 
					<span class="icon-bar"></span>
				</button>
				<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse offset"
					id="navbarSupportedContent">
					<ul class="nav navbar-nav menu_nav mr-auto">
						<li class="nav-item"><a class="nav-link"
							href="/ProjetBanqueS8/">Accueil</a></li>
						<li class="nav-item">
							<a class="nav-link" href="/ProjetBanqueS8/ActualiteBanque"> Actualités </a>
						</li>
						<li class="nav-item"><a class="nav-link" href="/ProjetBanqueS8/OffresBanque">Offres</a>
						<li class="nav-item"><a class="nav-link" href="/ProjetBanqueS8/Bourse">Bourse</a>
						<li class="nav-item"><a class="nav-link" href="/ProjetBanqueS8/FormulaireContact">Contact</a></li>
					</ul>
					<ul class="nav navbar-nav navbar-right">
						<li class="nav-item">
						<c:if test="${ !empty sessionScope.nom }">
						<a class="nom_prenom">Bonjour Mr ${ sessionScope.nom } ${ sessionScope.prenom }</a>
						<a class="btn btn-success" type="button" href="/ProjetBanqueS8/EspaceClient - Accueil">Mon Espace Client</a>
						<a class="btn btn-danger" type="button" href="/ProjetBanqueS8/DeconnexionBanque" >Deconnexion</a>
						</c:if>
						<c:if test="${ empty sessionScope.nom }">
						<a class="btn btn-info" type="button" href="/ProjetBanqueS8/InscriptionBanque">Créer un compte</a> 
						<a class="btn btn-success" type="button" href="/ProjetBanqueS8/ConnexionBanque">Espace Client</a>
						</c:if>
						</li>
					</ul>
					
				</div>
			</div>
		</nav>
	</div>
</header>