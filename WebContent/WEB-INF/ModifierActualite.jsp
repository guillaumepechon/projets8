<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page import="com.octest.dto.Actualite"%>
<%@ page import="java.util.ArrayList"%>

<!doctype html>
<html>
<head>
<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link rel="icon" href="img/favicon.png" type="image/png">
<title>My Bank - Ajout Actualité</title>
<!-- Bootstrap CSS -->
<link rel="stylesheet" href="css/bootstrap.css">
<link rel="stylesheet" href="vendors/linericon/style.css">
<link rel="stylesheet" href="css/font-awesome.min.css">
<link rel="stylesheet" href="vendors/owl-carousel/owl.carousel.min.css">
<link rel="stylesheet" href="vendors/lightbox/simpleLightbox.css">
<link rel="stylesheet" href="vendors/nice-select/css/nice-select.css">
<link rel="stylesheet" href="vendors/animate-css/animate.css">
<!-- main css -->
<link rel="stylesheet" href="css/style.css">
<link rel="stylesheet" href="css/responsive.css">
<link rel="stylesheet" href="css/StyleContactInscription.css" />
</head>

<body>
	<%@ include file="Nav.jsp"%>
	<%
		int idActu = Integer.parseInt(request.getParameter("idActu"));
		ArrayList<Actualite> listeActualite = (ArrayList<Actualite>) request.getAttribute("actualites");
	%>
	<section id="formulaire">
		<div class="container">
			<form method="post" action="ModifierActualite" enctype="multipart/form-data">
				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
							<label for="titre">Titre de l'article</label> <input required type="text"
								name="titre" class="form-control" id="titre" value="<%=listeActualite.get(idActu).getAct_title()%>">
						</div>
					</div>
					<div class="col-md-12">
						<div class="form-group">
							<label for="article">Article résumé</label>
							<textarea required id="article" name="article"
								class="form-control"><%=listeActualite.get(idActu).getAct_contenu().replace("<br/>", "")%></textarea>
						</div>
					</div>
					<div class="col-md-12">
						<div class="form-group">
							<label for="article_det">Article détaillé</label>
							<textarea required id="article_det" name="article_det"
								class="form-control"><%=listeActualite.get(idActu).getAct_contenu_det()%></textarea>
						</div>
					</div>
					<div class="col-md-12">
						<div class="form-group">
							<label for="fichier">Photo pour l'article : </label>
            				<input type="file" name="fichier" id="fichier" value="<%="/Users/christopher/eclipse-workspace/ProjetBanqueS8/WebContent/img"+listeActualite.get(idActu).getAct_photo()%>"/>
						</div>
					</div>
					<div class="col-md-12">
						<div class="checkbox">
							<label for="confirmation"><input required type="checkbox"
								name="confirmation" id="confirmation"> Veuillez confirmer
								votre ajout</label>
						</div>
					</div>
					<div class="col-md-12">
						<button type='submit' class='btn btn-primary' name="idActu" value="<%=listeActualite.get(idActu).getIdActu()%>">Envoyer</button>
					</div>
				</div>
			</form>
		</div>
	</section>
	<%@ include file="Footer.jsp"%>
</body>

</html>