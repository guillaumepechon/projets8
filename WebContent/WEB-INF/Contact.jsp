<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<!doctype html>
<html>
<head>
<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link rel="icon" href="img/favicon.png" type="image/png">
<title>My Bank - Contact</title>
<!-- Bootstrap CSS -->
<link rel="stylesheet" href="css/bootstrap.css">
<link rel="stylesheet" href="vendors/linericon/style.css">
<link rel="stylesheet" href="css/font-awesome.min.css">
<link rel="stylesheet" href="vendors/owl-carousel/owl.carousel.min.css">
<link rel="stylesheet" href="vendors/lightbox/simpleLightbox.css">
<link rel="stylesheet" href="vendors/nice-select/css/nice-select.css">
<link rel="stylesheet" href="vendors/animate-css/animate.css">
<!-- main css -->
<link rel="stylesheet" href="css/style.css">
<link rel="stylesheet" href="css/responsive.css">
<link rel="stylesheet" href="css/StyleContactInscription.css" />

<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
</head>

<body>
	<%@ include file="Nav.jsp"%>
	<section id="formulaire">
		<div class="container" id="info">
			<div class="row">
				<div class="col-md-12">
					<hr>
					<div class="alert alert-info">
						<p style="color:blue">
							<b style="color:blue">INFOS :</b> Pour toutes questions, vous pouvez nous contacter
							via ce formulaire. Nous vous répondrons au plus vite.
						</p>
					</div>
					<hr>
				</div>
			</div>
		</div>
		<div class="container">
			<form method="post" action="Contact">
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label for="nom">Votre nom</label> <input required type="text"
								name="nom" class="form-control" id="nom">
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label for="prenom">Votre prénom</label> <input required
								type="text" name="prenom" class="form-control" id="prenom">
						</div>
					</div>
					<div class="col-md-12">
						<div class="form-group">
							<label for="mail">Votre email</label> <input required
								type="email" name="mail" class="form-control" id="mail">
						</div>
					</div>
					<div class="col-md-12">
						<div class="form-group">
							<label for="message">Votre message</label>
							<textarea required id="message" name="message"
								class="form-control"></textarea>
						</div>
					</div>
					<div class="col-md-12">
						<div class="checkbox">
							<label for="confirmation"><input required type="checkbox"
								name="confirmation" id="confirmation">Veuillez confirmer
								votre demande</label>
						</div>
					</div>
					<div class="col-md-12">
						<button type='submit' class='btn btn-primary'>Envoyer</button>
					</div>
				</div>
			</form>
		</div>
	</section>
	<%@ include file="Footer.jsp"%>
</body>

</html>