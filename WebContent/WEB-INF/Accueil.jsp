<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<!doctype html>
<html lang="fr">
<head>
<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link rel="icon" href="img/favicon.png" type="image/png">
<title>My Bank - Accueil</title>
<!-- Bootstrap CSS -->
<link rel="stylesheet" href="css/bootstrap.css">
<link rel="stylesheet" href="vendors/linericon/style.css">
<link rel="stylesheet" href="css/font-awesome.min.css">
<link rel="stylesheet" href="vendors/owl-carousel/owl.carousel.min.css">
<link rel="stylesheet" href="vendors/lightbox/simpleLightbox.css">
<link rel="stylesheet" href="vendors/nice-select/css/nice-select.css">
<link rel="stylesheet" href="vendors/animate-css/animate.css">
<!-- main css -->
<link rel="stylesheet" href="css/style.css">
<link rel="stylesheet" href="css/responsive.css">

</head>
<body>

	<!--================Header Menu Area =================-->
	<%@ include file="Nav.jsp"%>
	<!--================Header Menu Area =================-->

	<!--================Home Banner Area =================-->
	<section class="home_banner_area">
		<div class="banner_inner d-flex align-items-center">
			<div class="overlay bg-parallax" data-stellar-ratio="0.9"
				data-stellar-vertical-offset="0" data-background=""></div>
			<div class="container">
				<div class="banner_content text-center">
					<h3>Rejoins notre banque en ligne</h3>
					<h5>Join Us</h5>
					<a class="org_btn" href="/ProjetBanqueS8/FormulaireContact">Contact</a>
					<a class="green_btn" href="/ProjetBanqueS8/InscriptionBanque">S'inscrire</a>
				</div>
				
			</div>
		</div>
	</section>
	<!--================End Home Banner Area =================-->

	<!--================Offer Area =================-->
	<section class="offer_area p_120">
		<div class="container">
			<div class="offer_title">
				<h5>La banque la moins chère</h5>
				<p>
					<strong>Vos cartes bancaires sont toujours gratuites.</br></strong> Mais
					aussi de nombreux services : tenue de compte, paiements et retraits
					en euros en France et à l'étranger ou encore prélèvements et
					virements en euros en France et en Zone SEPA... Et bien d'autres
					encore. Ce n'est pas pour rien que nous avons de nouveau été <strong>classée
						banque la moins chère en 2019</strong> pour la 11e année consécutive
				</p>
			</div>
			<div class="row offer_inner">
				<div class="col-lg-4">
					<div class="offer_item">
						<img class="img-fluid" src="img/offer/offer-1.jpg" alt="">
						<div class="offer_text">
							<h4>On est là pour vous</h4>
							<p>
								<strong>Une banque qui s'adapte à vous, ça change tout
									:</strong>des process simples, 100 % en ligne, sécurisés et toute la
								gamme de produits dont vous avez besoin au meilleur prix !
							</p>
						</div>
					</div>
				</div>
				<div class="col-lg-4">
					<div class="offer_item">
						<img class="img-fluid" src="img/offer/offer-2.jpg" alt="">
						<div class="offer_text">
							<h4>On est là pour vous</h4>
							<p>
								<strong>Reprenez la main sur la gestion de vos
									finances.</br>
								</strong> Retrouvez tous vos services en ligne 7j/7, 24h/24 et même à
								partir de votre mobile :
							<ul>
								<li>Ouverture de vos comptes simple, rapide et 100 % en
									ligne.</li>
								<li>Augmentation instantanée des plafonds de votre Carte
									Bancaire.
									<h6>(Réservé aux clients de plus de 3 mois. Sous réserve
										d'éligibilité)</h6>
								</li>
								<li>Personnalisation de vos notifications pour suivre en
									temps réel les opérations sur votre compte.</li>
							</ul>
							</p>

						</div>
					</div>
				</div>
				<div class="col-lg-4">
					<div class="offer_item">
						<img class="img-fluid" src="img/offer/offer-3.jpg" alt="">
						<div class="offer_text">
							<h4>On est là pour vous</h4>
							<p>
								Nos conseillers basés en France, sont disponibles par téléphone
								du lundi au vendredi de 8h à 22h et même le samedi de 8h45 à
								16h30, mais aussi par email ou par courrier.</br> Retrouvez également
								toutes les réponses à vos questions directement en ligne 24h/24.

								Des questions pour ouvrir votre compte, joignez nos conseillers
								commerciaux par tchat.
							</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!--================End Offer Area =================-->


	<!--================ start footer Area  =================-->
	<%@ include file="Footer.jsp"%>
	<!--================ End footer Area  =================-->



	<!-- Optional JavaScript -->
	<!-- jQuery first, then Popper.js, then Bootstrap JS -->
	<script src="js/jquery-3.2.1.min.js"></script>
	<script src="js/popper.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/stellar.js"></script>
	<script src="vendors/lightbox/simpleLightbox.min.js"></script>
	<script src="vendors/nice-select/js/jquery.nice-select.min.js"></script>
	<script src="vendors/isotope/imagesloaded.pkgd.min.js"></script>
	<script src="vendors/isotope/isotope-min.js"></script>
	<script src="vendors/owl-carousel/owl.carousel.min.js"></script>
	<script src="js/jquery.ajaxchimp.min.js"></script>
	<script src="vendors/counter-up/jquery.waypoints.min.js"></script>
	<script src="vendors/counter-up/jquery.counterup.js"></script>
	<script src="js/mail-script.js"></script>
	<script src="js/theme.js"></script>
</body>
</html>