<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport"
			content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<link rel="icon" href="img/favicon.png" type="image/png">
		<title>My Bank - Mon Espace Client</title>
		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="css/bootstrap.css">
		<link rel="stylesheet" href="vendors/linericon/style.css">
		<link rel="stylesheet" href="css/font-awesome.min.css">
		<link rel="stylesheet" href="vendors/owl-carousel/owl.carousel.min.css">
		<link rel="stylesheet" href="vendors/lightbox/simpleLightbox.css">
		<link rel="stylesheet" href="vendors/nice-select/css/nice-select.css">
		<link rel="stylesheet" href="vendors/animate-css/animate.css">
		<!-- main css -->
		<link rel="stylesheet" href="css/style.css">
		<link rel="stylesheet" href="css/responsive.css">
		<link rel="stylesheet" href="css/StyleMenuEspaceClient.css">
	</head>
	<body>
	<%@ include file="Nav.jsp"%>
	<div class="limiter">
	<div class="container-login100">
	<div class="row">
	  <div class="col-4">
		<div class="list-group">
		  <a href="/ProjetBanqueS8/EspaceClient - Profil" class="list-group-item active">Mon profil</a>
		  <a href="/ProjetBanqueS8/EspaceClient - Solde" class="list-group-item">Solde Compte</a>
		  <a href="#" class="list-group-item">Virements</a>
		  <a href="#" class="list-group-item">Alimentation</a>
		  <a href="#" class="list-group-item">Vestibulum at eros</a>
		</div>
	  </div>
	  
		<div class="col-8">
		  <h3>Vos Informations Personnelles</h3>
		  <a>Votre Nom : ${sessionScope.nom}</a>
		  <a>Votre Prenom : ${sessionScope.prenom}</a>
		  <br>
		  </br>
		  <a>Votre Email : ${sessionScope.email}</a>
		  <a>Votre Mot de Passe : ${sessionScope.mdp}</a>
		  <br>
		  </br>
		  <a>Votre Lieu de Naissance : ${sessionScope.lieu_naissance}</a>
		  <br>
		  </br>
		  <a>Votre Adresse Actuelle : ${sessionScope.adresse}</a>
		  <a>Votre Ville Actuelle : ${sessionScope.ville}</a>
		</div>
		<a class="btn btn-info" href="/ProjetBanqueS8//EspaceClient - Modifier Profil">Modifier</a>
		</div> 
		</div>
		</div>
	</body>
</html>