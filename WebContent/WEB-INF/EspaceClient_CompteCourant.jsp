<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link rel="icon" href="img/favicon.png" type="image/png">
<title>Espace Client - Mon Compte Courant</title>
<!-- Bootstrap CSS -->
<link rel="stylesheet" href="css/bootstrap.css">
<link rel="stylesheet" href="vendors/linericon/style.css">
<link rel="stylesheet" href="css/font-awesome.min.css">
<link rel="stylesheet" href="vendors/owl-carousel/owl.carousel.min.css">
<link rel="stylesheet" href="vendors/lightbox/simpleLightbox.css">
<link rel="stylesheet" href="vendors/nice-select/css/nice-select.css">
<link rel="stylesheet" href="vendors/animate-css/animate.css">
<!-- main css -->
<link rel="stylesheet" href="css/style.css">
<link rel="stylesheet" href="css/responsive.css">
<link rel="stylesheet" href="css/StyleMenuEspaceClient.css">
</head>
<body>
	<%@ include file="Nav.jsp"%>
	<div class="limiter">
		<div class="container-login100">
			<div class="row">
				<%@ include file="MenuEC.jsp"%>

				<div class="col-7">
					<div class="card" style="margin-top: 20px;">
						<div class="card-body" style="text-align: center;">
							<h3>Votre solde est de :</h3>
							<a>${sessionScope.solde} €</a>
						</div>
					</div>
					<div class="card" style="margin-top: 20px; background-color: grey;">
						<div class="card-body">
							<div class="row">
								<div class="col-sm-4">
									<div class="card" style="margin-top: 20px; text-align: center;">
										<div class="card-body">
											<h5 class="card-title" style="text-align: center;">Alimentation</h5>
											<p class="card-text" style="text-align: center;">Alimentez votre compte par Carte Bancaire, Chèque et Dêpot d'espèces</p>
											<a href="/ProjetBanqueS8/EspaceClient - Alimentation"
												class="btn btn-primary">Accéder</a>
										</div>
									</div>
								</div>
								<div class="col-sm-4">
									<div class="card" style="margin-top: 20px; text-align: center;">
										<div class="card-body">
											<h5 class="card-title" style="text-align: center;">Virement Interne</h5>
											<p class="card-text" style="text-align: center;">Effectuez un virement entre mes comptes (Courant, Epargne, Titre)</p>
											<a href="/ProjetBanqueS8/EspaceClient - Virement Interne"
												class="btn btn-primary">Accéder</a>
										</div>
									</div>
								</div>
								<div class="col-sm-4">
									<div class="card"
										style="margin-top: 20px; margin-bottom: 20px; text-align: center;">
										<div class="card-body">
											<h5 class="card-title" style="text-align: center;">Virement Externe</h5>
											<p class="card-text" style="text-align: center;">Effectuez un virement externe à l'aide d'un IBAN</p>
											<a href="/ProjetBanqueS8/VirementExterne"
												class="btn btn-primary">Accéder</a>
										</div>
									</div>
								</div>
								<div class="col-sm-4">
									<div class="card"
										style="margin-top: 20px; margin-bottom: 20px; text-align: center;">
										<div class="card-body">
											<h5 class="card-title" style="text-align: center;">Historique Transaction</h5>
											<p class="card-text" style="text-align: center;">Visualiser l'historique des transactions</p>
											<a href="/ProjetBanqueS8/HistTransactions"
												class="btn btn-primary">Accéder</a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>

</body>
</html>