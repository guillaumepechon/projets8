<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link rel="icon" href="img/favicon.png" type="image/png">
<title>Espace Client - Alimentation</title>
<!-- Bootstrap CSS -->
<link rel="stylesheet" href="css/bootstrap.css">
<link rel="stylesheet" href="vendors/linericon/style.css">
<link rel="stylesheet" href="css/font-awesome.min.css">
<link rel="stylesheet" href="vendors/owl-carousel/owl.carousel.min.css">
<link rel="stylesheet" href="vendors/lightbox/simpleLightbox.css">
<link rel="stylesheet" href="vendors/nice-select/css/nice-select.css">
<link rel="stylesheet" href="vendors/animate-css/animate.css">
<!-- main css -->
<link rel="stylesheet" href="css/style.css">
<link rel="stylesheet" href="css/responsive.css">
<link rel="stylesheet" href="css/StyleMenuEspaceClient.css">
</head>
<body>
	<%@ include file="Nav.jsp"%>
	<div class="limiter">
		<div class="container-login100">
			<div class="row">
				<%@ include file="MenuEC.jsp"%>

				<div class="col-6">
					<div class="card" style="margin-top: 20px; background-color: grey;">
						<div class="card-body">
							<div class="row">
								<div class="col-sm-4">
									<div class="card" style="margin-top: 20px; text-align: center;">
										<div class="card-body">
											<h5 class="card-title" style="text-align: center;">Compte
												Courant</h5>
											<p class="card-text" style="text-align: center;">Votre
												Solde est de : ${sessionScope.solde_compte}€</p>
											<a href="/ProjetBanqueS8/EspaceClient - Compte Courant"
												class="btn btn-primary">Voir Compte Courant</a>
										</div>
									</div>
								</div>
								<div class="col-sm-4">
									<div class="card" style="margin-top: 20px; text-align: center;">
										<div class="card-body">
											<h5 class="card-title" style="text-align: center;">Compte
												Epargne</h5>
											<p class="card-text" style="text-align: center;">Votre
												Solde est de : ${sessionScope.solde_epargne}€</p>
											<a href="/ProjetBanqueS8/EspaceClient - Compte Epargne"
												class="btn btn-primary">Voir Compte Epargne</a>
										</div>
									</div>
								</div>
								<div class="col-sm-4">
									<div class="card"
										style="margin-top: 20px; margin-bottom: 20px; text-align: center;">
										<div class="card-body">
											<h5 class="card-title" style="text-align: center;">Compte
												Titre</h5>
											<p class="card-text" style="text-align: center;">Votre
												Solde est de : ${sessionScope.solde_titre}€</p>
											<a href="/ProjetBanqueS8/EspaceClient - Compte Titre"
												class="btn btn-primary">Voir Compte Titre</a>
										</div>
									</div>
								</div>
								
								<iframe width="800" height="600" src="https://app.powerbi.com/view?r=eyJrIjoiYjI2ZWZlNTYtZTcyNy00ODVhLTk3OWEtZTAzN2FmNTgzMDdiIiwidCI6IjM3MWNiMTU2LTk1NTgtNDI4Ni1hM2NkLTMwNTk2OTliODkwYyIsImMiOjh9" frameborder="0" allowFullScreen="true"></iframe>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

</body>
</html>