<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link rel="icon" href="img/favicon.png" type="image/png">
<title>Espace Client - Création Compte Epargne</title>
<!-- Bootstrap CSS -->
<link rel="stylesheet" href="css/bootstrap.css">
<link rel="stylesheet" href="vendors/linericon/style.css">
<link rel="stylesheet" href="css/font-awesome.min.css">
<link rel="stylesheet" href="vendors/owl-carousel/owl.carousel.min.css">
<link rel="stylesheet" href="vendors/lightbox/simpleLightbox.css">
<link rel="stylesheet" href="vendors/nice-select/css/nice-select.css">
<link rel="stylesheet" href="vendors/animate-css/animate.css">
<!-- main css -->
<link rel="stylesheet" href="css/style.css">
<link rel="stylesheet" href="css/responsive.css">
<link rel="stylesheet" href="css/StyleMenuEspaceClient.css">
</head>
<body>
	<%@ include file="Nav.jsp"%>
	<div class="limiter">
		<div class="container-login100">
			<div class="row">
				<%@ include file="MenuEC.jsp"%>
				<div class="col-7">
					<div class="card" style="margin-top: 20px;">
						<div class="card-body" style="text-align: center;">
						<form method="POST" action="EspaceClient - Creer Compte Epargne">
						        <h3 >Création Compte Epargne</h3>
								<div class="form-group">
									<label class="form-type" for="type"> Choisissez le type de compte épargne que vous souhaitez ouvrir: </label> 
									<select class="form-control sel" name="type">
									   <option value="Livret_A">Livret A</option>
									   <option value="Livret_Jeune">Livret Jeune</option>
									   <option value="PEL">Plan Epargne Logement</option>
									</select>
								</div>
								<div class="form-group">
								    <input type="submit" name ="submit" class="btn btn-success" value="Créer le Compte Epargne">
								</div>
							</form>
							</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>