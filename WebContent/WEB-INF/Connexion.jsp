<%@ page pageEncoding="UTF-8"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="fr">
<head>
<meta charset="UTF-8">
			<title>My Bank - Connexion</title>
			<link rel="icon" href="img/favicon.png" type="image/png">
			<link rel="stylesheet" href="css/bootstrap.css">
	        <link rel="stylesheet" href="vendors/linericon/style.css">
	        <link rel="stylesheet" href="css/font-awesome.min.css">
	        <link rel="stylesheet" href="vendors/owl-carousel/owl.carousel.min.css">
	        <link rel="stylesheet" href="vendors/lightbox/simpleLightbox.css">
	        <link rel="stylesheet" href="vendors/nice-select/css/nice-select.css">
	        <link rel="stylesheet" href="vendors/animate-css/animate.css">
	        <!-- main css -->
	        <link rel="stylesheet" href="css/style.css">
	        <link rel="stylesheet" href="css/StyleConnexion.css">
	        <link rel="stylesheet" href="css/responsive.css">
</head>
<body>
	<%@ include file="Nav.jsp"%>

	<div class="limiter">
				<div class="container-login100">
					<div class="wrap-login100 p-l-85 p-r-85 p-t-55 p-b-55">
					     <span class="login100-form-title">
								Connexion aux comptes 
						 </span>
					<form method="post" action="Connexion">
		                 <div class="form-group">
							<label class= "form-type" for="email"> Email </label>
						    <input type="email" class="form-control" name ="email" id="email" placeholder="Entrez votre email">
					     </div>
						 
						 <div class="form-group">
						     <label class= "form-type" for="mdp"> Mot de Passe </label> 
						     <input type="password" class="form-control" name="mdp" id="mdp" placeholder="Entrez votre Mot de Passe">
						 </div>				
				
						<div class="form-group">
							<button type="submit" class="btn btn-success btval"> Valider </button>
					    </div>
		
						</form>
						</div>
					</div>
				</div>

	<%@ include file="Footer.jsp"%>
</body>
</html>