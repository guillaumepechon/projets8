package com.octest.dto;

import java.sql.Date;


public class Actualite {

	public int idActu;
	public Date act_date;
	public String act_title;
	public String act_contenu;
	public String act_photo;
	public String act_contenu_det;

	public Actualite(int idActu, Date act_date, String act_title, String act_contenu, String act_photo, String act_contenu_det){
		this.idActu = idActu;
		this.act_date = act_date;
		this.act_title = act_title;
		this.act_contenu = act_contenu;
		this.act_photo = act_photo;
		this.act_contenu_det = act_contenu_det;
	}

	public int getIdActu() {
		return idActu;
	}

	public void setIdActu(int idActu) {
		this.idActu = idActu;
	}

	public Date getAct_date() {
		return act_date;
	}

	public void setAct_date(Date act_date) {
		this.act_date = act_date;
	}

	public String getAct_title() {
		return act_title;
	}

	public void setAct_title(String act_title) {
		this.act_title = act_title;
	}

	public String getAct_contenu() {
		return act_contenu;
	}

	public void setAct_contenu(String act_photo) {
		this.act_photo = act_photo;
	}
	public String getAct_photo() {
		return act_photo;
	}

	public void setAct_photo(String act_photo) {
		this.act_photo = act_photo;
	}
	public String getAct_contenu_det() {
		return act_contenu_det;
	}

	public void setAct_contenu_det(String act_contenu_det) {
		this.act_contenu_det = act_contenu_det;
	}

}