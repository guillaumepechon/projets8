package com.octest.dto;


public class Client {
	private int idClient;
	private String civi;
	private String nom;
	private String prenom;
	private String email;
	private String mot_de_passe;
	private String date_naissance;
	private String ville_naissance;
	private String pays_naissance;
	private String nation;
	private String adresse;
	private String ville;
	private String pays;
	private String tel;
	
	public Client(int idClient, String civi, String nom, String prenom, String email, String mot_de_passe, String ville_naissance, String pays_naissance, String nation, String adresse, String ville, String pays, String tel) {
		this.idClient = idClient; 
		this.civi = civi;
		this.nom = nom;
		this.prenom = prenom;
		this.email=email;
		this.mot_de_passe=mot_de_passe;
		this.ville_naissance = ville_naissance;
		this.pays_naissance = pays_naissance;
		this.nation = nation;
		this.adresse = adresse;
		this.ville = ville;
		this.pays = pays;
		this.tel = tel;
	}
	
	public int getIdClient() {
		return idClient;
	}

	public void setIdClient(int idClient) {
		this.idClient = idClient;
	}
    
	public String getCivi() {
		return civi;
	}

	public void setCivi(String civi) {
		this.civi = civi;
	}
	
	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
    
	public String getEmail() {
		return email;
	}
	
	public void setEmail(String mail) {
		email=mail;
	}
	
	public String getMot_de_passe() {
		return mot_de_passe;
	}

	public void setMot_de_passe(String mot_de_passe) {
		this.mot_de_passe = mot_de_passe;
	}
    
	public String getVille_naissance() {
		return ville_naissance;
	}

	public void setVille_naissance(String ville_naissance) {
		this.ville_naissance = ville_naissance;
	}

	public String getPays_naissance() {
		return pays_naissance;
	}

	public void setPays_naissance(String pays_naissance) {
		this.pays_naissance = pays_naissance;
	}

	public String getNation() {
		return nation;
	}

	public void setNation(String nation) {
		this.nation = nation;
	}

	public String getAdresse() {
		return adresse;
	}

	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}

	public String getVille() {
		return ville;
	}

	public void setVille(String ville) {
		this.ville = ville;
	}

	public String getPays() {
		return pays;
	}

	public void setPays(String pays) {
		this.pays = pays;
	}

	public String getTel() {
		return tel;
	}

	public void setTel(String tel) {
		this.tel = tel;
	}
}
