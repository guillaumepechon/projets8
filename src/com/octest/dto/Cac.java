package com.octest.dto;

public class Cac {

	private String nom= null;
	private String ouverture = null;
	private String haut = null;
	private String bas = null;
	private String volume = null;
	private String veille = null;
	private String dernier = null;
	private String var = null;
	
	public Cac(String nom, String ouverture, String haut, String bas, String volume, String veille, String dernier, String var) {
		this.nom = nom;
		this.ouverture = ouverture;
		this.haut = haut;
		this.bas = bas;
		this.volume = volume;
		this.veille = veille;
		this.dernier = dernier;
		this.var = var;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getOuverture() {
		return ouverture;
	}

	public void setOuverture(String ouverture) {
		this.ouverture = ouverture;
	}

	public String getHaut() {
		return haut;
	}

	public void setHaut(String haut) {
		this.haut = haut;
	}

	public String getBas() {
		return bas;
	}

	public void setBas(String bas) {
		this.bas = bas;
	}

	public String getVolume() {
		return volume;
	}

	public void setVolume(String volume) {
		this.volume = volume;
	}

	public String getVeille() {
		return veille;
	}

	public void setVeille(String veille) {
		this.veille = veille;
	}

	public String getDernier() {
		return dernier;
	}

	public void setDernier(String dernier) {
		this.dernier = dernier;
	}

	public String getVar() {
		return var;
	}

	public void setVar(String var) {
		this.var = var;
	}
   
	
}
