package com.octest.dto;


public class OffreCompte {

	public int idoffrecompte;
	public String type_compte;
	public String description_compte;
	public String photo_compte;

	public OffreCompte(int idoffrecompte, String type_compte, String description_compte, String photo_compte){
		this.idoffrecompte = idoffrecompte;
		this.type_compte = type_compte;
		this.description_compte = description_compte;
		this.photo_compte = photo_compte;
	}

	public int getIdOffreCompte() {
		return idoffrecompte;
	}

	public void setIdOffreCompte(int idoffrecompte) {
		this.idoffrecompte = idoffrecompte;
	}

	public String getTypeCompte() {
		return type_compte;
	}

	public void setTypeCompte(String type_compte) {
		this.type_compte = type_compte;
	}

	public String getDescriptionCompte() {
		return description_compte;
	}

	public void setDescriptionCompte(String description_compte) {
		this.description_compte = description_compte;
	}
	
	public String getPhotoCompte() {
		return photo_compte;
	}

	public void setPhotoCompte(String photo_compte) {
		this.photo_compte = photo_compte;
	}

}