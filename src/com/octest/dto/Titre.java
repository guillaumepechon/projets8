package com.octest.dto;

public class Titre {
  private int idTitre;
  private int idCompte;
  private float solde;
  
  public Titre(int idTitre, int idCompte, float solde) {
	  this.idTitre = idTitre;
	  this.idCompte = idCompte;
	  this.solde = solde;
  }

public int getIdTitre() {
	return idTitre;
}

public void setIdTitre(int idTitre) {
	this.idTitre = idTitre;
}

public int getIdCompte() {
	return idCompte;
}

public void setIdCompte(int idCompte) {
	this.idCompte = idCompte;
}

public float getSolde() {
	return solde;
}

public void setSolde(float solde) {
	this.solde = solde;
}
  
  
}
