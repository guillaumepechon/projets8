package com.octest.dto;


public class Offre {

	public int idOffre;
	public String off_description;
	public String off_tarif;

	public Offre(int idOffre, String off_description, String off_tarif){
		this.idOffre = idOffre;
		this.off_description = off_description;
		this.off_tarif = off_tarif;
	}

	public int getIdOffre() {
		return idOffre;
	}

	public void setIdOffre(int idOffre) {
		this.idOffre = idOffre;
	}
	
	public String getOff_description() {
		return off_description;
	}
	
	public void setOff_description(String off_description) {
		this.off_description = off_description;
	}

	public String getOff_tarif() {
		return off_tarif;
	}

	public void setOff_tarif(String off_tarif) {
		this.off_tarif = off_tarif;
	}

}
