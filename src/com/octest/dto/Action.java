package com.octest.dto;

public class Action {

	private int idAction;
	private int idTitre;
	private String nom;
	private int nombre;
	private float prix; 
	
	public Action(int idAction, int idTitre, String nom, int nombre, float prix) {
		this.idAction = idAction;
		this.idTitre = idTitre;
		this.nom = nom;
		this.nombre = nombre;
		this.prix = prix;
	}

	public int getIdAction() {
		return idAction;
	}

	public void setIdAction(int idAction) {
		this.idAction = idAction;
	}

	public int getIdTitre() {
		return idTitre;
	}

	public void setIdTitre(int idTitre) {
		this.idTitre = idTitre;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public int getNombre() {
		return nombre;
	}

	public void setNombre(int nombre) {
		this.nombre = nombre;
	}

	public float getPrix() {
		return prix;
	}

	public void setPrix(float prix) {
		this.prix = prix;
	}
	
	

}
