package com.octest.dto;

import java.util.Date;

public class Transactions {

	private int idTrans;
	private int trans_idCompte;
	private int trans_idCompte_emetteur;
	private int trans_idCompte_recepteur;
	private Date trans_date;
	private String trans_type;
	private float trans_montant;
	private String trans_motif;
	
	public Transactions(int idTrans, int trans_idCompte, int trans_idCompte_emetteur, int trans_idCompte_recepteur, Date trans_date, String trans_type, float trans_montant, String trans_motif) {
		this.idTrans = idTrans;
		this.trans_idCompte = trans_idCompte;
		this.trans_idCompte_emetteur = trans_idCompte_emetteur;
		this.trans_idCompte_recepteur = trans_idCompte_recepteur;
		this.trans_date = trans_date;
		this.trans_type = trans_type;
		this.trans_montant = trans_montant;
		this.trans_motif = trans_motif;
	}
	
	public int getIdTrans() {
		return idTrans;
	}
	public void setIdTrans(int idTrans) {
		this.idTrans=idTrans;
	}
	
	public int getTransIdCompte() {
		return trans_idCompte;
	}
	public void setTransIdCompte(int trans_idCompte) {
		this.trans_idCompte=trans_idCompte;
	}
	
	public int getTransIdCompteE() {
		return trans_idCompte_emetteur;
	}
	public void setTransIdCompteE(int trans_idCompte_emetteur) {
		this.trans_idCompte_emetteur=trans_idCompte_emetteur;
	}
	
	public int getTransIdCompteR() {
		return trans_idCompte_recepteur;
	}
	public void setTransIdCompteR(int trans_idCompte_recepteur) {
		this.trans_idCompte_recepteur=trans_idCompte_recepteur;
	}
	
	public Date getTransDate() {
		return trans_date;
	}
	public void setTransDate(Date trans_date) {
		this.trans_date=trans_date;
	}
	
	public String getTransType() {
		return trans_type;
	}
	public void setTransType(String trans_type) {
		this.trans_type=trans_type;
	}
	
	public float getTransMontant() {
		return trans_montant;
	}
	public void setTransMontant(float trans_montant) {
		this.trans_montant=trans_montant;
	}
	
	public String getTransMotif() {
		return trans_motif;
	}
	public void setTransMotif(String trans_motif) {
		this.trans_motif=trans_motif;
	}
}
