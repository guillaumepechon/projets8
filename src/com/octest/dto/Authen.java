package com.octest.dto;

public class Authen {

	private String nom;
	private String prenom;
	private String email;
	private String mot_de_passe;
	private String date_naissance;
	private String lieu_naissance;
	private String adresse;
	private String ville;
	
	public Authen(String nom, String prenom, String email, String mot_de_passe, String date_naissance, String lieu_naissance, String adresse, String ville) {
		this.nom = nom;
		this.prenom = prenom;
		this.email=email;
		this.mot_de_passe=mot_de_passe;
		this.date_naissance = date_naissance;
		this.lieu_naissance = lieu_naissance;
		this.adresse = adresse;
		this.ville = ville;
	}
	
	
	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
    
	public String getEmail() {
		return email;
	}
	
	public void setEmail(String mail) {
		email=mail;
	}
	
	public String getMot_de_passe() {
		return mot_de_passe;
	}

	public void setMot_de_passe(String mot_de_passe) {
		this.mot_de_passe = mot_de_passe;
	}

	public String getDate_naissance() {
		return date_naissance;
	}

	public void setDate_naissance(String date_naissance) {
		this.date_naissance = date_naissance;
	}

	public String getLieu_naissance() {
		return lieu_naissance;
	}

	public void setLieu_naissance(String lieu_naissance) {
		this.lieu_naissance = lieu_naissance;
	}

	public String getAdresse() {
		return adresse;
	}

	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}

	public String getVille() {
		return ville;
	}

	public void setVille(String ville) {
		this.ville = ville;
	}
}
