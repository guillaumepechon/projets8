package com.octest.dto;


public class OffreCarte {

	public int idoffrecarte;
	public String type_carte;
	public String description_carte;
	public String photo_carte;

	public OffreCarte(int idoffrecarte, String type_carte, String description_carte, String photo_carte){
		this.idoffrecarte = idoffrecarte;
		this.type_carte = type_carte;
		this.description_carte = description_carte;
		this.photo_carte = photo_carte;
	}

	public int getIdOffreCarte() {
		return idoffrecarte;
	}

	public void setIdOffreCarte(int idoffrecarte) {
		this.idoffrecarte = idoffrecarte;
	}

	public String getTypeCarte() {
		return type_carte;
	}

	public void setTypeCarte(String type_carte) {
		this.type_carte = type_carte;
	}

	public String getDescriptionCarte() {
		return description_carte;
	}

	public void setDescriptionCarte(String description_carte) {
		this.description_carte = description_carte;
	}
	
	public String getPhotoCarte() {
		return photo_carte;
	}

	public void setPhotoCarte(String photo_carte) {
		this.photo_carte = photo_carte;
	}

}