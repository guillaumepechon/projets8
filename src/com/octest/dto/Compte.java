package com.octest.dto;


public class Compte {

	private int idCompte;
	private int idClient;
	private String offre;
	private float solde;
	private String iban;
    
	public Compte(int idCompte,int idClient,String offre,float solde, String iban) {
    	this.idCompte=idCompte;
    	this.idClient=idClient;
    	this.offre=offre;
    	this.solde=solde;
    	this.iban=iban;
	}

	public int getIdCompte() {
		return idCompte;
	}

	public void setIdCompte(int idCompte) {
		this.idCompte = idCompte;
	}

	public int getIdClient() {
		return idClient;
	}

	public void setIdClient(int idClient) {
		this.idClient = idClient;
	}

	public String getOffre() {
		return offre;
	}

	public void setOffre(String offre) {
		this.offre = offre;
	}

	public float getSolde() {
		return solde;
	}

	public void setSolde(float solde) {
		this.solde = solde;
	}

	public String getIban() {
		return iban;
	}

	public void setIban(String iban) {
		this.iban = iban;
	}
    
	
}


