package com.octest.dto;

public class Epargne {
     private int idEpargne;
     private int idCompte;
     private String type;
     private float solde;
     private float taux; 
     
     public Epargne(int idEpargne, int idCompte, String type, float solde, float taux) {
    	 this.idEpargne = idEpargne; 
    	 this.idCompte = idCompte;
    	 this.type = type;
    	 this.solde = solde;
    	 this.taux = taux;
     }

	public int getIdEpargne() {
		return idEpargne;
	}

	public void setIdEpargne(int idEpargne) {
		this.idEpargne = idEpargne;
	}

	public int getIdCompte() {
		return idCompte;
	}

	public void setIdCompte(int idCompte) {
		this.idCompte = idCompte;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public float getSolde() {
		return solde;
	}

	public void setSolde(float solde) {
		this.solde = solde;
	}

	public float getTaux() {
		return taux;
	}

	public void setTaux(float taux) {
		this.taux = taux;
	}
     
     
}
