package com.octest.servlets;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import com.octest.models.OffreDAO;

/**
 * Servlet implementation class ModifierActualite
 */
@WebServlet("/ModifierOffre")
public class ModifierOffre extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ModifierOffre() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		OffreDAO mydao = new OffreDAO();
		request.setAttribute("offres", mydao.RecupererListeOffre());
		this.getServletContext().getRequestDispatcher("/WEB-INF/ModifierOffre.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String description = request.getParameter("description");
		String tarif = request.getParameter("tarif");
		int idOffre = Integer.parseInt(request.getParameter("idOffre"));
		String msg = null;
        
        OffreDAO mydao = new OffreDAO();
        if (description.isEmpty() || tarif.isEmpty()) {
			msg="Veuillez completer tous les champs";
			
		}else {
			if(mydao.modifierOffre(idOffre, description, tarif)) {
				msg="Le formulaire a bien été enregistré";
			}else {
				msg="L'enregistrement du formulaire à échoué";
			}
		}
        // redirects client to message page
        
        getServletContext().getRequestDispatcher("/WEB-INF/Accueil.jsp").forward(
                request, response);
    }
}

