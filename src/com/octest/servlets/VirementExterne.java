package com.octest.servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.octest.models.VirementExterneDAO;

/**
 * Servlet implementation class VirementExterne
 */
@WebServlet("/VirementExterne")
public class VirementExterne extends HttpServlet {
	private static final long serialVersionUID = 1L;
	int idClient;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public VirementExterne() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		this.getServletContext().getRequestDispatcher("/WEB-INF/VirementExterne.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String iban = request.getParameter("iban");
		float montant = Float.parseFloat(request.getParameter("montant"));
		String motif = request.getParameter("motif");
		String msg = null;
		HttpSession session = request.getSession(true);
		idClient=(int)session.getAttribute("idClient");
		VirementExterneDAO mydao = new VirementExterneDAO();
		if (iban.isEmpty() || motif.isEmpty() || montant<=0) {
			msg="Veuillez completer ou vérifier tous les champs";
			
		}else {
			if(mydao.virementExterne(idClient, iban, montant, motif)) {
				msg="Le virement a été effectué";
			}else {
				msg="Le virement a échoué";
			}
		}
		request.setAttribute("Reponse", msg);
		this.getServletContext().getRequestDispatcher("/WEB-INF/Accueil.jsp").forward(request, response);
		
	}

}