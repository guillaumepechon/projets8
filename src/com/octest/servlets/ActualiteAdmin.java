package com.octest.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.octest.models.ActualiteDAO;
/**
 * Servlet implementation class ActualiteAdmin
 */
@WebServlet("/ActualiteAdmin")
public class ActualiteAdmin extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ActualiteAdmin() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		ActualiteDAO mydao = new ActualiteDAO();
		request.setAttribute("actualites", mydao.RecupererListeActualite());
		this.getServletContext().getRequestDispatcher("/WEB-INF/ActualiteAdmin.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String msg = null;
		ActualiteDAO mydao = new ActualiteDAO();
		if(request.getParameter("idActu") != null) {
			int idOffre = Integer.parseInt(request.getParameter("idActu"));
			if(mydao.suppressionActualite(idOffre)) {
					msg="L'ajout a bien fonctionné";
			}else {
				msg="L'enregistrement à échoué";
			}
		}
		request.setAttribute("Reponse", msg);
		this.getServletContext().getRequestDispatcher("/WEB-INF/Accueil.jsp").forward(request, response);
		
	}
}