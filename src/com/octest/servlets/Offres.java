package com.octest.servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.octest.models.OffreDAO;
import com.octest.models.OffreCarteDAO;
import com.octest.models.OffreCompteDAO;

/**
 * Servlet implementation class Offres
 */
@WebServlet("/Offres")
public class Offres extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Offres() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		OffreDAO mydao = new OffreDAO();
		OffreCarteDAO mydao2 = new OffreCarteDAO();
		OffreCompteDAO mydao3 = new OffreCompteDAO();
		request.setAttribute("offres", mydao.RecupererListeOffre());
		request.setAttribute("offrescartes", mydao2.RecupererListeOffreCarte());
		request.setAttribute("offrescomptes", mydao3.RecupererListeOffreCompte());
		this.getServletContext().getRequestDispatcher("/WEB-INF/Offres.jsp").forward(request, response);
	
	}
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
