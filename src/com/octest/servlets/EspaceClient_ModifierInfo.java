package com.octest.servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.octest.dto.Client;
import com.octest.models.ModInfoDAO;

/**
 * Servlet implementation class EspaceClient_ModifierInfo
 */
@WebServlet("/EspaceClient - Modifier Profil")
public class EspaceClient_ModifierInfo extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public EspaceClient_ModifierInfo() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		this.getServletContext().getRequestDispatcher("/WEB-INF/EspaceClient_ModifierProfil.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		HttpSession session = request.getSession();
		int idClient = (int) session.getAttribute("idClient");
		String nom = request.getParameter("nom");
		String prenom = request.getParameter("prenom");
		String mail = request.getParameter("mail");
		String mot_de_passe = request.getParameter("mot_de_passe");
		String date_naissance = request.getParameter("date_naissance");
		java.sql.Date newDate = java.sql.Date.valueOf(date_naissance);
		String lieu_naissance  = request.getParameter("lieu_naissance");
		String adresse = request.getParameter("adresse");
		String ville = request.getParameter("ville");
		String msg = null;
		ModInfoDAO myDAO = new ModInfoDAO();
		if (nom.isEmpty() || prenom.isEmpty() || mail.isEmpty() || mot_de_passe.isEmpty() || date_naissance.isEmpty() || lieu_naissance.isEmpty() || adresse.isEmpty() || ville.isEmpty()) {
			msg="Veuillez completer tous les champs";
		} else {
			if(myDAO.modInfo(nom, prenom, mail, mot_de_passe, date_naissance, lieu_naissance, adresse, ville, idClient)){
				msg="Les modifications ont �taient effectu�";
		        session.setAttribute("nom", nom);
		        session.setAttribute("prenom", prenom);
		        session.setAttribute("email", mail);
		        session.setAttribute("mdp", mot_de_passe);
		        session.setAttribute("date_naissance", newDate);
		        session.setAttribute("lieu_naissance", lieu_naissance);
		        session.setAttribute("adresse", adresse);
		        session.setAttribute("ville", ville);
			}else {
	        msg="La modification n'a pas fonctionn�";
		}
		}
		 request.setAttribute("Reponse", msg);
		 this.getServletContext().getRequestDispatcher("/WEB-INF/EspaceClient_Profil.jsp").forward(request, response);
		}
	}
