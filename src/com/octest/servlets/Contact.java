package com.octest.servlets;

import java.io.IOException;
import java.sql.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.octest.models.ContactDAO;
import com.octest.servlets.send_email_gmail;

/**
 * Servlet implementation class Contact
 */
@WebServlet("/Contact")
public class Contact extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Contact() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		this.getServletContext().getRequestDispatcher("/WEB-INF/Contact.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String nom = request.getParameter("nom");
		String prenom = request.getParameter("prenom");
		String mail = request.getParameter("mail");
		String message = request.getParameter("message");
		String msg = null;
		ContactDAO mydao = new ContactDAO();
		send_email_gmail mail2 = new send_email_gmail();
		if (nom.isEmpty() || prenom.isEmpty() || mail.isEmpty() || message.isEmpty()) {
			msg="Veuillez completer tous les champs";
			
		}else {
			if(mydao.ajoutFormulaireContact(nom, prenom, mail, message)) {
				msg="Le formulaire a bien été enregistré";
				mail2.envoieMail(mail, prenom, nom);
			}else {
				msg="L'enregistrement du formulaire à échoué";
			}
		}
		request.setAttribute("Reponse", msg);
		this.getServletContext().getRequestDispatcher("/WEB-INF/Accueil.jsp").forward(request, response);
		
	}

}
