package com.octest.servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.octest.dto.Titre;
import com.octest.models.TitreDAO;

/**
 * Servlet implementation class EspaceClient_CompteTitre
 */
@WebServlet("/EspaceClient - Compte Titre")
public class EspaceClient_CompteTitre extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public EspaceClient_CompteTitre() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		HttpSession session = request.getSession();
		int idCompte = (int) session.getAttribute("idCompte");
		Titre ti = null;
        TitreDAO myDAOti = new TitreDAO();
        if(!myDAOti.compteOK(idCompte)){
        ti = myDAOti.getTitre(idCompte);
        request.setAttribute("idTitre", ti.getIdTitre());
        session.setAttribute("idTitre", ti.getIdTitre());
        session.setAttribute("solde_titre", ti.getSolde());
        }
        
        this.getServletContext().getRequestDispatcher("/WEB-INF/EspaceClient_CompteTitre.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		this.getServletContext().getRequestDispatcher("/WEB-INF/EspaceClient_CompteTitre.jsp").forward(request, response);
	}

}
