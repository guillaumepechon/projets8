package com.octest.servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.octest.dto.*;
import com.octest.models.EpargneDAO;

/**
 * Servlet implementation class EspaceClient_CreerCompteEpargne
 */
@WebServlet("/EspaceClient - Creer Compte Epargne")
public class EspaceClient_CreerCompteEpargne extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public EspaceClient_CreerCompteEpargne() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		this.getServletContext().getRequestDispatcher("/WEB-INF/EspaceClient_CreerCompteEpargne.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		HttpSession session = request.getSession();
		int idCompte = (int) session.getAttribute("idCompte");
		String type = request.getParameter("type");
		EpargneDAO myDAO = new EpargneDAO(); 
		String msg = null;
		Float solde = 0.0f;
		Float taux = 2.0f;
		Epargne epa = null;

		if(type == "Livret_A"){
			taux = 3.75f;	
		}else {
			if(type == "Livret_Jeune"){
				taux = 0.02f;
			}else {
				if(type == "PEL") {
					taux = 0.01f;
				}else {
					msg = "Veuillez choisir un type d'épargne";
				}
			}
		}	

		
		if(myDAO.creerCompte(idCompte, type, solde, taux)) {
			msg="Votre compte titre est crée";
			epa = myDAO.getEpargne(idCompte);
			session.setAttribute("idEpargne", epa.getIdEpargne());
			session.setAttribute("type", epa.getType());
			session.setAttribute("solde_epargne", epa.getSolde());
			session.setAttribute("taux", epa.getTaux());
		}
		request.setAttribute("msg", msg);
		this.getServletContext().getRequestDispatcher("/WEB-INF/EspaceClient_Accueil.jsp").forward(request, response);
	}

}
