package com.octest.servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.octest.dto.Compte;
import com.octest.models.CompteDAO;

/**
 * Servlet implementation class EspaceClient_Alim
 */
@WebServlet("/EspaceClient - Alimentation")
public class EspaceClient_Alim extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public EspaceClient_Alim() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		this.getServletContext().getRequestDispatcher("/WEB-INF/EspaceClient_Alim.jsp").forward(request, response);
		
		
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		HttpSession session = request.getSession();
		int idclient = (int) session.getAttribute("idClient");
		float solde = (float) session.getAttribute("solde");
		String msg = null;
		Float newsolde = 0.0f;
		Float montant = Float.parseFloat(request.getParameter("montant"));
		newsolde = solde + montant;
		CompteDAO myDAO = new CompteDAO();
		if (myDAO.alimCompte(idclient, newsolde)) {
			msg = null; 
		}else {
			msg = "L'alimentation du compte n'a pas fonctionné";
		}
		request.setAttribute("msg", msg);
		request.setAttribute("solde", newsolde);
		session.setAttribute("solde", newsolde);
		this.getServletContext().getRequestDispatcher("/WEB-INF/EspaceClient_Alim.jsp").forward(request, response);		
	}

}
