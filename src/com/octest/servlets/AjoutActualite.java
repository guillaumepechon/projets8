package com.octest.servlets;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
 
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
 
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.FilenameUtils;

import com.octest.models.ActualiteDAO;


/**
 * Servlet implementation class AjoutActualite
 */
public class AjoutActualite extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private static final String UPLOAD_DIRECTORY = "img";
	private static final int MEMORY_THRESHOLD   = 1024 * 1024 * 3;  // 3MB
    private static final int MAX_FILE_SIZE      = 1024 * 1024 * 40; // 40MB
    private static final int MAX_REQUEST_SIZE   = 1024 * 1024 * 50; // 50MB
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AjoutActualite() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		this.getServletContext().getRequestDispatcher("/WEB-INF/AjoutActualite.jsp").forward(request, response);
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String titre = null;
		String article = null;
		String article_det = null;
		String fileDirectory = null;
		String msg = null;
        if (!ServletFileUpload.isMultipartContent(request)) {
            // if not, we stop here
            PrintWriter writer = response.getWriter();
            writer.println("Error: Form must has enctype=multipart/form-data.");
            writer.flush();
            return;
        }
        
 
        // configures upload settings
        DiskFileItemFactory factory = new DiskFileItemFactory();
        // sets memory threshold - beyond which files are stored in disk
        factory.setSizeThreshold(MEMORY_THRESHOLD);
        // sets temporary location to store files
        factory.setRepository(new File(System.getProperty("java.io.tmpdir")));
 
        ServletFileUpload upload = new ServletFileUpload(factory);
         
        // sets maximum size of upload file
        upload.setFileSizeMax(MAX_FILE_SIZE);
         
        // sets maximum size of request (include file + form data)
        upload.setSizeMax(MAX_REQUEST_SIZE);
 
        // constructs the directory path to store upload file
        // this path is relative to application's directory
        String uploadPath = getServletContext().getRealPath("") + UPLOAD_DIRECTORY;
         
        // creates the directory if it does not exist
        File uploadDir = new File(uploadPath);
        if (!uploadDir.exists()) {
            uploadDir.mkdir();
        }
 
        try {
            // parses the request's content to extract file data
            @SuppressWarnings("unchecked")
            List<FileItem> formItems = upload.parseRequest(request);
 
            if (formItems != null && formItems.size() > 0) {
                // iterates over form's fields
                for (FileItem item : formItems) {
                    // processes only fields that are not form fields
                    if (!item.isFormField()) {
                        String fileName = new File(item.getName()).getName();
                        String extension = FilenameUtils.getExtension(fileName);
                        String nomFich = FilenameUtils.getBaseName(fileName);
                        Date date = new Date();
                        DateFormat formatDate = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss");
                        String datefin = formatDate.format(date);
                        String filePath = uploadPath + File.separator + nomFich + "_" + datefin + "." + extension;
                        
                        fileDirectory = nomFich + "_" + datefin + "." + extension;
                        File storeFile = new File(filePath);
 
                        // saves the file on disk
                        item.write(storeFile);
                        request.setAttribute("message",
                            "Upload has been done successfully!");
                    }else {
                    	String fieldName = item.getFieldName();
    			        String fieldValue = item.getString("UTF-8");
    			        if (fieldName.equals("titre")) {
    			        	titre=fieldValue;
    			        }else if(fieldName.equals("article")) {
    			        	article=fieldValue;
    			        }else if(fieldName.equals("article_det")) {
    			        	article_det=fieldValue;
    			        }
                    }
                }
            }
        } catch (Exception ex) {
            request.setAttribute("message",
                    "There was an error: " + ex.getMessage());
        }
        ActualiteDAO mydao = new ActualiteDAO();
        if (titre.isEmpty() || article.isEmpty() || article_det.isEmpty() || fileDirectory.isEmpty()) {
			msg="Veuillez completer tous les champs";
			
		}else {
			if(mydao.ajoutActualite(titre, article, fileDirectory, article_det)) {
				msg="Le formulaire a bien été enregistré";
			}else {
				msg="L'enregistrement du formulaire à échoué";
			}
		}
        // redirects client to message page
        
        getServletContext().getRequestDispatcher("/WEB-INF/Accueil.jsp").forward(
                request, response);
    }
}