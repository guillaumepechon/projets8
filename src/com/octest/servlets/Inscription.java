package com.octest.servlets;

import java.io.IOException;
import java.util.*;
import java.text.*;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.octest.models.InscriptionDAO;

/**
 * Servlet implementation class Inscription
 */
@WebServlet("/Inscription")
public class Inscription extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Inscription() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		this.getServletContext().getRequestDispatcher("/WEB-INF/Inscription.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String civi = request.getParameter("civi");
		String offre = request.getParameter("offre");
		String nom = request.getParameter("nom");
		String prenom = request.getParameter("prenom");
		String mail = request.getParameter("mail");
		String mail2 = request.getParameter("mail2");
		String mot_de_passe = request.getParameter("mot_de_passe");
		String conf_mdp = request.getParameter("conf_mdp");
		String date_naissance = request.getParameter("date_naissance");
		String ville_naissance  = request.getParameter("ville_naissance");
		String pays_naissance  = request.getParameter("pays_naissance");
		String nation = request.getParameter("nation");
		String adresse = request.getParameter("adresse");
		String ville = request.getParameter("ville");
		String pays = request.getParameter("pays");
		String tel = request.getParameter("tel");
		String msg = null;
		Float solde = 0.0f;
		int idclient = 0;
		InscriptionDAO mydao = new InscriptionDAO();
		if (civi.isEmpty() || nom.isEmpty() || prenom.isEmpty() || mail.isEmpty() || mail2.isEmpty() || mot_de_passe.isEmpty() || conf_mdp.isEmpty() || date_naissance.isEmpty() || ville_naissance.isEmpty() || pays_naissance.isEmpty() || nation.isEmpty() || adresse.isEmpty() || ville.isEmpty() || pays.isEmpty() || tel.isEmpty() ) {
			msg="Veuillez completer tous les champs";
			
		}else {
			if(mydao.ajoutInscription(civi, nom, prenom, mail, mot_de_passe, date_naissance, ville_naissance, pays_naissance, nation, adresse, ville, pays, tel)) {
				msg="Vous �tes maintenant inscrit";
				idclient = mydao.getId(mail, mot_de_passe);
				if(mydao.creerCompte(idclient, offre, solde)){
					msg="Votre compte est crée";
				}
				msg="L'inscription n'a pas fonctionnée";
			}
		}
		request.setAttribute("Reponse", msg);
		this.getServletContext().getRequestDispatcher("/WEB-INF/Accueil.jsp").forward(request, response);
	}

}
