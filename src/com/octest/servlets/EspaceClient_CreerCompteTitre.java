package com.octest.servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.octest.dto.*;
import com.octest.models.TitreDAO;


/**
 * Servlet implementation class EspaceClient_CreerCompteTitre
 */
@WebServlet("/EspaceClient - Creer Compte Titre")
public class EspaceClient_CreerCompteTitre extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public EspaceClient_CreerCompteTitre() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		this.getServletContext().getRequestDispatcher("/WEB-INF/EspaceClient_CreerCompteTitre.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		HttpSession session = request.getSession();
		int idCompte = (int) session.getAttribute("idCompte");
		TitreDAO myDAO = new TitreDAO();
		String msg = null;
		Float solde = 0.0f;
		Titre ti = null;
			if(myDAO.creerCompte(idCompte, solde)){
				msg="Votre compte titre est crée";
				ti = myDAO.getTitre(idCompte);
				session.setAttribute("idTitre", ti.getIdTitre());
				session.setAttribute("solde_titre", ti.getSolde());
			}
		request.setAttribute("msg", msg);
		this.getServletContext().getRequestDispatcher("/WEB-INF/EspaceClient_Accueil.jsp").forward(request, response);
	}

}
