package com.octest.servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.octest.models.CompteDAO;

/**
 * Servlet implementation class InterneEpargne
 */
@WebServlet("/InterneEpargneBanque")
public class InterneEpargne extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public InterneEpargne() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		this.getServletContext().getRequestDispatcher("/WEB-INF/InterneEpargne.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
		String emet= "Compte Epargne";
		String rec="Compte Courant";
		//String date= request.getParameter("date");
		float cash = Float.parseFloat(request.getParameter("cash"));
		String motif= request.getParameter("motif");
		String msg = null;
		CompteDAO mydao = new CompteDAO();
		
		HttpSession session = request.getSession();
		int idClient= (int)session.getAttribute("idClient");
		
		if ( cash == 0 || motif.isEmpty()) {
			msg="Veuillez completer tous les champs";
			
		}else {
				if(mydao.virIntCompte(idClient, emet, rec, cash, motif)) {
					msg="Le virement a bien �t� effectu�";
				}else {
					msg="Le virement a �chou�";
				}
			}
		request.setAttribute("Reponse", msg);
		this.getServletContext().getRequestDispatcher("/WEB-INF/VirementInterne.jsp").forward(request, response);
		
	
	
	}

}
