package com.octest.servlets;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.octest.models.OffreCarteDAO;
import com.octest.models.OffreCompteDAO;
import com.octest.models.OffreDAO;
import com.octest.dto.Offre;

import au.com.bytecode.opencsv.CSVWriter;

/**
 * Servlet implementation class OffreAdmin
 */
@WebServlet("/OffreAdmin")
public class OffreAdmin extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public OffreAdmin() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		OffreDAO mydao = new OffreDAO();
		OffreCarteDAO mydao2 = new OffreCarteDAO();
		OffreCompteDAO mydao3 = new OffreCompteDAO();
		request.setAttribute("offres", mydao.RecupererListeOffre());
		request.setAttribute("offrescartes", mydao2.RecupererListeOffreCarte());
		request.setAttribute("offrescomptes", mydao3.RecupererListeOffreCompte());
		this.getServletContext().getRequestDispatcher("/WEB-INF/OffresAdmin.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String msg = null;
		OffreDAO mydao = new OffreDAO();
		OffreCarteDAO mydao2 = new OffreCarteDAO();
		OffreCompteDAO mydao3 = new OffreCompteDAO();
		if(request.getParameter("idOffre") != null) {
			int idOffre = Integer.parseInt(request.getParameter("idOffre"));
			if(mydao.suppressionOffre(idOffre)) {
					msg="L'ajout a bien fonctionné";
			}else {
				msg="L'enregistrement à échoué";
			}
		}else if(request.getParameter("idOffreCarte") != null) {
			int idOffreCarte = Integer.parseInt(request.getParameter("idOffreCarte"));
			if(mydao2.suppressionOffreCarte(idOffreCarte)) {
				msg="L'ajout a bien fonctionné";
			}else {
				msg="L'enregistrement à échoué";
			}
		}else if(request.getParameter("idOffreCompte") != null) {
			int idOffreCompte = Integer.parseInt(request.getParameter("idOffreCompte"));
			if(mydao3.suppressionOffreCompte(idOffreCompte)) {
				msg="L'ajout a bien fonctionné";
			}else {
				msg="L'enregistrement à échoué";
			}
		}
		request.setAttribute("Reponse", msg);
		this.getServletContext().getRequestDispatcher("/WEB-INF/Accueil.jsp").forward(request, response);
		
	}
}
