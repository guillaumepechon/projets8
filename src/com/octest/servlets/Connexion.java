package com.octest.servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.octest.dto.*;
import com.octest.models.CompteDAO;
import com.octest.models.ConnexionDAO;
import com.octest.models.EpargneDAO;
import com.octest.models.TitreDAO;



/**
 * Servlet implementation class Connexion
 */
@WebServlet("/Connexion")
public class Connexion extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Connexion() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		this.getServletContext().getRequestDispatcher("/WEB-INF/Connexion.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		String email = request.getParameter("email");
		String mdp = request.getParameter("mdp");
		String nom = null;
		String msg = null;
		Client client = null;
		Compte compte = null;
		ConnexionDAO myDAO = new ConnexionDAO();
		CompteDAO mydao = new CompteDAO();
		if (email.isEmpty() || mdp.isEmpty()|| myDAO.getNom(email, mdp)==null) {
			msg="Veuillez completer tous les champs";
		} else {
			client = myDAO.getClient(email, mdp);
            int idClient = client.getIdClient();
            compte = mydao.getCompte(idClient);
	        session.setAttribute("idClient", client.getIdClient());
	        session.setAttribute("civi", client.getCivi());
	        session.setAttribute("nom", client.getNom());
	        session.setAttribute("prenom", client.getPrenom());
	        session.setAttribute("email", client.getEmail());
	        session.setAttribute("mdp", client.getMot_de_passe());
	        session.setAttribute("ville_naissance", client.getVille_naissance());
	        session.setAttribute("pays_naissance", client.getPays_naissance());
	        session.setAttribute("nation", client.getNation());
	        session.setAttribute("adresse", client.getAdresse());
	        session.setAttribute("ville", client.getVille());
	        session.setAttribute("pays", client.getPays());
	        session.setAttribute("tel", client.getTel());
	        session.setAttribute("idCompte", compte.getIdCompte());
	        session.setAttribute("cpt_offre", compte.getOffre());
	        session.setAttribute("solde_courant", compte.getSolde());
	        int idCompte = compte.getIdCompte();
	        Epargne epa = null;
	     	EpargneDAO myDAOepa = new EpargneDAO();
	        if(myDAOepa.compteOK(idCompte)) {
	            epa = myDAOepa.getEpargne(idCompte);
	            session.setAttribute("idEpargne", epa.getIdEpargne());
	            session.setAttribute("type_epa", epa.getType());
	            session.setAttribute("solde_epargne", epa.getSolde());
	            session.setAttribute("taux_epa", epa.getTaux());
	        }
	        
	        Titre ti = null;
	        TitreDAO myDAOti = new TitreDAO();
	        if(myDAOti.compteOK(idCompte)){
	        ti = myDAOti.getTitre(idCompte);
	        session.setAttribute("idTitre", ti.getIdTitre());
	        session.setAttribute("solde_titre", ti.getSolde());
	        }
	        msg ="Vous êtes maintenant connecté";
		}

		 request.setAttribute("Reponse", msg);
		 this.getServletContext().getRequestDispatcher("/WEB-INF/EspaceClient_Accueil.jsp").forward(request, response);
		}
	}


