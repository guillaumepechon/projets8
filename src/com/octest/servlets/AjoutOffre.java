package com.octest.servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.octest.models.OffreDAO;

/**
 * Servlet implementation class AjoutOffre
 */
@WebServlet("/AjoutOffre")
public class AjoutOffre extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AjoutOffre() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		this.getServletContext().getRequestDispatcher("/WEB-INF/AjoutOffre.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String description = request.getParameter("description");
		String tarif = request.getParameter("tarif");
		String msg = null;
		OffreDAO mydao = new OffreDAO();
		if (description.isEmpty() || tarif.isEmpty()) {
			msg="Veuillez completer tous les champs";
			
		}else {
			if(mydao.ajoutOffre(description, tarif)) {
				msg="L'ajout a bien fonctionné";
			}else {
				msg="L'enregistrement à échoué";
			}
		}
		request.setAttribute("Reponse", msg);
		this.getServletContext().getRequestDispatcher("/WEB-INF/Accueil.jsp").forward(request, response);
		
	}

}
