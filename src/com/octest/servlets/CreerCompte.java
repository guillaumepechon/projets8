package com.octest.servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.octest.models.CompteDAO;

/**
 * Servlet implementation class CreerCompte
 */
@WebServlet("/CreerCompte")
public class CreerCompte extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CreerCompte() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		this.getServletContext().getRequestDispatcher("/WEB-INF/CreerCompte.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String nom = request.getParameter("nom");
		String prenom = request.getParameter("prenom");
		String mail = request.getParameter("mail");
		String mot_de_passe = request.getParameter("mot_de_passe");
		String type_compte = request.getParameter("menu_compte");
		String msg = null;
		CompteDAO mydao = new CompteDAO();
		if (nom.isEmpty() || prenom.isEmpty() || mail.isEmpty() || mot_de_passe.isEmpty() || type_compte.isEmpty()) {
			msg="Veuillez completer tous les champs";
			
		}else {
			if(mydao.nombreCompte(nom, prenom, mail, mot_de_passe) >= 2) {
				msg="Vous avez déjà un compte courant et un compte épargne";
			}else {
				if(mydao.ajouterCompte(nom, prenom, mail, mot_de_passe, type_compte)) {
					msg="Votre compte a bien été créé";
				}else {
					msg="La création de votre compte a échoué";
				}
			}
		}
		request.setAttribute("Reponse", msg);
		this.getServletContext().getRequestDispatcher("/WEB-INF/Accueil.jsp").forward(request, response);
	}

}
