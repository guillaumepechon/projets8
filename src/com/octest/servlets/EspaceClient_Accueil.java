package com.octest.servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.octest.dto.*;
import com.octest.models.*;

/**
 * Servlet implementation class EspaceClient_Accueil
 */
@WebServlet("/EspaceClient - Accueil")
public class EspaceClient_Accueil extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public EspaceClient_Accueil() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		HttpSession session = request.getSession();
        Compte cpt = null;
        int idclient = (int) session.getAttribute("idClient");
        CompteDAO myDAOcpt = new CompteDAO();
        cpt = myDAOcpt.getCompte(idclient);
        session.setAttribute("idCompte", cpt.getIdCompte());
        session.setAttribute("solde_compte", cpt.getSolde());
        session.setAttribute("offre", cpt.getOffre());
        int idCompte = cpt.getIdCompte();
        Epargne epa = null;
     	EpargneDAO myDAOepa = new EpargneDAO();
        if(myDAOepa.compteOK(idCompte)) {
            epa = myDAOepa.getEpargne(idCompte);
            session.setAttribute("idEpargne", epa.getIdEpargne());
            session.setAttribute("type_epa", epa.getType());
            session.setAttribute("solde_epargne", epa.getSolde());
            session.setAttribute("taux_epa", epa.getTaux());
        }
        
        Titre ti = null;
        TitreDAO myDAOti = new TitreDAO();
        if(myDAOti.compteOK(idCompte)){
        ti = myDAOti.getTitre(idCompte);
        session.setAttribute("idTitre", ti.getIdTitre());
        session.setAttribute("solde_titre", ti.getSolde());
        }
        
        this.getServletContext().getRequestDispatcher("/WEB-INF/EspaceClient_Accueil.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		this.getServletContext().getRequestDispatcher("/WEB-INF/EspaceClient_Accueil.jsp").forward(request, response);
	}

}
