package com.octest.servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.io.FileWriter;
import java.io.StringWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
 
import au.com.bytecode.opencsv.CSVWriter;

import com.octest.dto.Transactions;
import com.octest.models.HistTransactionsDAO;


@WebServlet("/HistTransactions")
public class HistTransactions extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public HistTransactions() {
        super();
    }
    int idClient;
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
    
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println("doGet Debut");
		HistTransactionsDAO mydao = new HistTransactionsDAO();
		HttpSession session = request.getSession(true);
		idClient=(int)session.getAttribute("idClient");
		System.out.println("doGet");
		request.setAttribute("histTransactions", mydao.RecupererHistTransactions(idClient));
		this.getServletContext().getRequestDispatcher("/WEB-INF/HistTransactions.jsp").forward(request, response);
		System.out.println("doGet Fin");
	}
    /*
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HistTransactionsDAO mydao = new HistTransactionsDAO();
		HttpSession session = request.getSession(true);
		idClient=(int)session.getAttribute("idClient");
		System.out.println(idClient);
		request.setAttribute("clientToCompte", mydao.ClientToCompte(idClient));
		this.getServletContext().getRequestDispatcher("/WEB-INF/HistTransactions.jsp").forward(request, response);
	}*/

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		CSVWriter writer = new CSVWriter(new FileWriter("C:\\Users\\guill\\OneDrive\\Bureau\\historiqueTransaction.csv"), ';');
	    HistTransactionsDAO mydao = new HistTransactionsDAO();
	    HttpSession session = request.getSession(true);
		idClient=(int)session.getAttribute("idClient");
     	ArrayList<Transactions> listeTransactions = mydao.RecupererHistTransactions(idClient);
     	String[][] Transactions= new String[listeTransactions.size()+1][7];
     	Transactions[0][0]="Montant de la transaction";
     	Transactions[0][1]="ID de la transaction";
     	Transactions[0][2]="ID compte emetteur";
     	Transactions[0][3]="ID compte recepteur";
     	Transactions[0][4]="Date de la transaction";
     	Transactions[0][5]="Type de la transaction";
     	Transactions[0][6]="Motif de la transaction";
     	for(int i=1; i<listeTransactions.size()+1; i++) {
     		for(int j=0; j<7; j++) {
     			if(j==0) {
     				Transactions[i][j]=Float.toString(listeTransactions.get(i-1).getTransMontant());
     			}else if(j==1) {
     				Transactions[i][j]=Integer.toString(listeTransactions.get(i-1).getIdTrans());
     			}else if(j==2) {
     				Transactions[i][j]=Integer.toString(listeTransactions.get(i-1).getTransIdCompteE());
     			}else if(j==3) {
     				Transactions[i][j]=Integer.toString(listeTransactions.get(i-1).getTransIdCompteR());
     			}else if(j==4) {
     				DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
     				String Date = dateFormat.format(listeTransactions.get(i-1).getTransDate());  
     				Transactions[i][j]=Date;
     			}else if(j==5) {
     				Transactions[i][j]=listeTransactions.get(i-1).getTransType();
     			}else if(j==6) {
     				Transactions[i][j]=listeTransactions.get(i-1).getTransMotif();
     			}
     		}
     	}
     	for(String elem[]:Transactions) {
     		writer.writeNext(elem);
     	}	
		writer.close();
		this.getServletContext().getRequestDispatcher("/WEB-INF/Accueil.jsp").forward(request, response);

	}

}
