package com.octest.servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.octest.dto.Epargne;
import com.octest.models.EpargneDAO;

/**
 * Servlet implementation class EspaceClient_CompteEpargne
 */
@WebServlet("/EspaceClient - Compte Epargne")
public class EspaceClient_CompteEpargne extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public EspaceClient_CompteEpargne() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		HttpSession session = request.getSession();
		int idCompte = (int) session.getAttribute("idCompte");
		Epargne epa = null;
        EpargneDAO myDAOepa = new EpargneDAO();
        epa = myDAOepa.getEpargne(idCompte);
        request.setAttribute("idEpargne", epa.getIdEpargne());
        session.setAttribute("idEpargne", epa.getIdEpargne());
        session.setAttribute("type_epa", epa.getType());
        session.setAttribute("solde_epargne", epa.getSolde());
        session.setAttribute("taux_epa", epa.getTaux());
        this.getServletContext().getRequestDispatcher("/WEB-INF/EspaceClient_CompteEpargne.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		this.getServletContext().getRequestDispatcher("/WEB-INF/EspaceClient_CompteEpargne.jsp").forward(request, response);
	}

}
