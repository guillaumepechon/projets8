package com.octest.servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.octest.dto.Compte;
import com.octest.models.CompteDAO;

/**
 * Servlet implementation class EspaceClient_CompteCourant
 */
@WebServlet("/EspaceClient - Compte Courant")
public class EspaceClient_CompteCourant extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public EspaceClient_CompteCourant() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		HttpSession session = request.getSession();
        Compte cpt = null;
        int idclient = (int) session.getAttribute("idClient");
        CompteDAO myDAO = new CompteDAO();
        cpt = myDAO.getCompte(idclient);
        session.setAttribute("idCompte", cpt.getIdCompte());
        session.setAttribute("solde", cpt.getSolde());
        session.setAttribute("offre", cpt.getOffre());
        this.getServletContext().getRequestDispatcher("/WEB-INF/EspaceClient_CompteCourant.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		this.getServletContext().getRequestDispatcher("/WEB-INF/EspaceClient_CompteCourant.jsp").forward(request, response);
	}

}
