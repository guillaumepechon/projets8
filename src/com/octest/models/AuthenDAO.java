package com.octest.models;

import java.sql.*;


public class AuthenDAO {
	/**
	 * parametres de connection a la base de donnees oracle URL,LOGIN et PASS
	 * sont des constantes
	 */
	static final String JDBC_DRIVER = "com.mysql.cj.jdbc.Driver";  
    static final String DB_URL = "jdbc:mysql://localhost/projets8?useSSL=true&serverTimezone=Europe/Paris";
    static final String USER = "root";
    static final String PASS = "mddc1260";
    
	/**
	* Constructor de la classe
	*/
	public AuthenDAO() {
		 //chargement du pilote de bases de donnees
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
		}catch(ClassNotFoundException e) {
			System.err.println("Impossible de charger le pilote de BDD, ne pas oublier d'importer le ficher .jar dans le projet");
		}
	}
	
	/**
	* Permet de recuperer le mot de passe a partir de son identifiant
	*
	* @param login le login de gestionnaire
	* @return gestionnaire trouv��;
	* null si aucun gestionnaire ne correspond �� cet identifiant
	*/
	public String getMdpAuthen(String email){
		Connection conn = null;
		PreparedStatement ps =null;
		ResultSet rs = null;
		String retour = null;
		try {
			Class.forName("com.mysql.jdbc.Driver");
			conn = DriverManager.getConnection(DB_URL,USER,PASS);
			ps = conn.prepareStatement("SELECT cli_mdp FROM client WHERE cli_email = ?");
			ps.setString(1,email);
			// on execute la requete
			// rs contient un pointeur situe juste avant la premiere ligne
			// retournee
			rs = ps.executeQuery();
			// passe a la premiere (et unique) ligne retournee
			if (rs.next())
				retour = rs.getString("cli_mdp");
		}catch (Exception ee){
			ee.printStackTrace();
		}finally{
			// fermeture du ResultSet, du PreparedStatement et de la Connexion
			try{
				if (rs != null)
					rs.close();
			}catch (Exception ignore){
				
			}
			
			try{
				if (ps != null)
					ps.close();
			}catch (Exception ignore){
				
			}
			
			try{
				if (conn != null)
					conn.close();
			}catch (Exception ignore){
				
			}
		}
		return retour;
	}
}