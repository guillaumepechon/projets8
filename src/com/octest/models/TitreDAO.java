package com.octest.models;
import java.sql.*;
import java.util.*;
import com.octest.dto.*;

public class TitreDAO {
	private static String URL = "jdbc:mysql://localhost/projets8?useSSL=true&serverTimezone=Europe/Paris";
	private static String UTILISATEUR = "root";
	private static String MOTDEPASSE = "mddc1260";
	
	public TitreDAO() {
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}	// TODO Auto-generated constructor stub
	}
	
	public boolean compteOK(int idCompte) {
		int idTitre = 0;
		Connection con = null;
		PreparedStatement ps = null;
        ResultSet rs = null;
        try {
			con = DriverManager.getConnection(URL, UTILISATEUR, MOTDEPASSE);
			ps = con.prepareStatement("SELECT idTitre FROM titre WHERE ti_idCompte = ?");
			ps.setInt(1, idCompte);
			rs=ps.executeQuery();
			
			while(rs.next()) {
			    idTitre=rs.getInt(1);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			try {
				if (ps != null) {
					ps.close();
				}
			} catch (Exception ignore) {
			}
			try {
				if (con != null) {
					con.close();
				}
			} catch (Exception ignore) {
			}
			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception ignore) {
			}
		}
		return true;
	}
	
	public boolean creerCompte(int idCompte, float solde){
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = DriverManager.getConnection(URL, UTILISATEUR, MOTDEPASSE);
			ps = con.prepareStatement("INSERT INTO titre (ti_idCompte, ti_solde) VALUES (?, ?)");
			ps.setInt(1, idCompte);
			ps.setFloat(2, solde);
			ps.executeUpdate();
			
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			try {
				if (ps != null) {
					ps.close();
				}
			} catch (Exception ignore) {
			}
			try {
				if (con != null) {
					con.close();
				}
			} catch (Exception ignore) {
			}
		}
		return true;
	}
	
	public boolean alimCompte(int idTitre, float solde) {
		PreparedStatement ps = null;
		Connection con = null;
		
		try {
			    con = DriverManager.getConnection(URL, UTILISATEUR, MOTDEPASSE);
				ps = con.prepareStatement("Update titre SET ti_solde = ? WHERE idTitre = ?");
				ps.setFloat(1, solde);
				ps.setInt(2, idTitre);
				ps.executeUpdate();
				
                
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			try {
				if (ps != null) {
					ps.close();
				}
			} catch (Exception ignore) {
			}
			try {
				if (con != null) {
					con.close();
				}
			} catch (Exception ignore) {
			}
		}
		return true;
	}
	
	public Titre getTitre(int idCompte) {
		Titre ti = null;
		Connection con = null;
		PreparedStatement ps = null;
        ResultSet rs = null;
        try {
			con = DriverManager.getConnection(URL, UTILISATEUR, MOTDEPASSE);
			ps = con.prepareStatement("SELECT idTitre, ti_idCompte, ti_solde FROM titre WHERE ti_idCompte = ?");
			ps.setInt(1, idCompte);
			rs=ps.executeQuery();
			
			while(rs.next()) {
				ti = new Titre(rs.getInt(1), rs.getInt(2), rs.getFloat(3));
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (ps != null) {
					ps.close();
				}
			} catch (Exception ignore) {
			}
			try {
				if (con != null) {
					con.close();
				}
			} catch (Exception ignore) {
			}
			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception ignore) {
			}
		}
		return ti;
	}

}
