package com.octest.models;

import java.io.InputStream;
import java.sql.Connection;
import java.util.Date;

import javax.servlet.http.Part;

import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import com.octest.dto.OffreCompte;

public class OffreCompteDAO {
	
	private static String URL = "jdbc:mysql://localhost/projets8?useSSL=false&serverTimezone=Europe/Paris";
	private static String UTILISATEUR = "root";
	private static String MOTDEPASSE = "mddc1260";
	

	public OffreCompteDAO() {
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	
	public boolean ajoutOffreCompte(String type_compte, String description_compte, String photo_compte) {
		Connection con = null;
		PreparedStatement ps = null;

		try {
			con = DriverManager.getConnection(URL, UTILISATEUR, MOTDEPASSE);
			ps = con.prepareStatement("INSERT INTO offrecompte (type_compte,description_compte,photo_compte) VALUES (?,?,?)");
			ps.setString(1, type_compte);
			ps.setString(2, description_compte);
			ps.setString(3, photo_compte);
			ps.executeUpdate();
			
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			try {
				if (ps != null) {
					ps.close();
				}
			} catch (Exception ignore) {
			}
			try {
				if (con != null) {
					con.close();
				}
			} catch (Exception ignore) {
			}
		}
		return true;
	}
	
	public boolean modifierOffreCompte(int idoffrecompte, String type_compte, String description_compte, String photo_compte) {
		Connection con = null;
		PreparedStatement ps = null;

		try {
			con = DriverManager.getConnection(URL, UTILISATEUR, MOTDEPASSE);
			ps = con.prepareStatement("UPDATE offrecompte SET type_compte=?,description_compte=?,photo_compte=? WHERE idoffrecompte=?");
			ps.setString(1, type_compte);
			ps.setString(2, description_compte);
			ps.setString(3, photo_compte);
			ps.setInt(4, idoffrecompte);
			ps.executeUpdate();
			
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			try {
				if (ps != null) {
					ps.close();
				}
			} catch (Exception ignore) {
			}
			try {
				if (con != null) {
					con.close();
				}
			} catch (Exception ignore) {
			}
		}
		return true;
	}
	
	public ArrayList<OffreCompte> RecupererListeOffreCompte() {

		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList<OffreCompte> listeOffreCompte = new ArrayList<OffreCompte>();

		// connexion à la base de données
		try {

			con = DriverManager.getConnection(URL, UTILISATEUR, MOTDEPASSE);
			ps = con.prepareStatement("SELECT * FROM offrecompte");

			// on exécute la requête
			rs = ps.executeQuery();
			// on parcourt les lignes du résultat
			while (rs.next()) {
				listeOffreCompte.add(
						new OffreCompte(rs.getInt("idoffrecompte"), rs.getString("type_compte"), rs.getString("description_compte"), rs.getString("photo_compte")));
			}
			
		} catch (Exception ee) {
			ee.printStackTrace();
		} finally {
			// fermeture du rs, du preparedStatement et de la connexion
			try {
				if (rs != null)
					rs.close();
			} catch (Exception ignore) {
			}
			try {
				if (ps != null)
					ps.close();
			} catch (Exception ignore) {
			}
			try {
				if (con != null)
					con.close();
			} catch (Exception ignore) {
			}
		}
		return listeOffreCompte;

	}
	
	public ArrayList<OffreCompte> rechercheOffreCompte(String mot_cle) {

		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList<OffreCompte> listeOffreCompte = new ArrayList<OffreCompte>();

		// connexion à la base de données
		try {

			con = DriverManager.getConnection(URL, UTILISATEUR, MOTDEPASSE);
			ps = con.prepareStatement("SELECT * FROM offrecompte WHERE type_compte LIKE ? OR description_compte LIKE ?");
			ps.setString(1, '%'+mot_cle+'%');
			ps.setString(2, '%'+mot_cle+'%');
			
			// on exécute la requête
			rs = ps.executeQuery();
			// on parcourt les lignes du résultat
			while (rs.next()) {
				listeOffreCompte.add(
						new OffreCompte(rs.getInt("idoffrecompte"), rs.getString("type_compte"), rs.getString("description_compte"), rs.getString("photo_compte")));
			}
			
		} catch (Exception ee) {
			ee.printStackTrace();
		} finally {
			// fermeture du rs, du preparedStatement et de la connexion
			try {
				if (rs != null)
					rs.close();
			} catch (Exception ignore) {
			}
			try {
				if (ps != null)
					ps.close();
			} catch (Exception ignore) {
			}
			try {
				if (con != null)
					con.close();
			} catch (Exception ignore) {
			}
		}
		return listeOffreCompte;

	}
	
	public boolean suppressionOffreCompte(int idOffreCompte) {
		Connection con = null;
		PreparedStatement ps = null;

		try {
			con = DriverManager.getConnection(URL, UTILISATEUR, MOTDEPASSE);
			ps = con.prepareStatement("DELETE FROM offrecompte WHERE idoffrecompte=?");
			ps.setInt(1, idOffreCompte);
			ps.executeUpdate();
			
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			try {
				if (ps != null) {
					ps.close();
				}
			} catch (Exception ignore) {
			}
			try {
				if (con != null) {
					con.close();
				}
			} catch (Exception ignore) {
			}
		}
		return true;
	}
}