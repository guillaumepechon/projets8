package com.octest.models;

import java.io.InputStream;
import java.sql.Connection;
import java.util.Date;

import javax.servlet.http.Part;

import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import com.octest.dto.OffreCarte;

public class OffreCarteDAO {
	
	private static String URL = "jdbc:mysql://localhost/projets8?useSSL=false&serverTimezone=Europe/Paris";
	private static String UTILISATEUR = "root";
	private static String MOTDEPASSE = "mddc1260";
	

	public OffreCarteDAO() {
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	
	public boolean ajoutOffreCarte(String type_carte, String description_carte, String photo_carte) {
		Connection con = null;
		PreparedStatement ps = null;

		try {
			con = DriverManager.getConnection(URL, UTILISATEUR, MOTDEPASSE);
			ps = con.prepareStatement("INSERT INTO offrecarte (type_carte,description_carte,photo_carte) VALUES (?,?,?)");
			ps.setString(1, type_carte);
			ps.setString(2, description_carte);
			ps.setString(3, photo_carte);
			ps.executeUpdate();
			
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			try {
				if (ps != null) {
					ps.close();
				}
			} catch (Exception ignore) {
			}
			try {
				if (con != null) {
					con.close();
				}
			} catch (Exception ignore) {
			}
		}
		return true;
	}
	
	public boolean modifierOffreCarte(int idoffrecarte, String type_carte, String description_carte, String photo_carte) {
		Connection con = null;
		PreparedStatement ps = null;

		try {
			con = DriverManager.getConnection(URL, UTILISATEUR, MOTDEPASSE);
			ps = con.prepareStatement("UPDATE offrecarte SET type_carte=?,description_carte=?,photo_carte=? WHERE idoffrecarte=?");
			ps.setString(1, type_carte);
			ps.setString(2, description_carte);
			ps.setString(3, photo_carte);
			ps.setInt(4, idoffrecarte);
			ps.executeUpdate();
			
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			try {
				if (ps != null) {
					ps.close();
				}
			} catch (Exception ignore) {
			}
			try {
				if (con != null) {
					con.close();
				}
			} catch (Exception ignore) {
			}
		}
		return true;
	}
	
	public ArrayList<OffreCarte> RecupererListeOffreCarte() {

		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList<OffreCarte> listeOffreCarte = new ArrayList<OffreCarte>();

		// connexion à la base de données
		try {

			con = DriverManager.getConnection(URL, UTILISATEUR, MOTDEPASSE);
			ps = con.prepareStatement("SELECT * FROM offrecarte");

			// on exécute la requête
			rs = ps.executeQuery();
			// on parcourt les lignes du résultat
			while (rs.next()) {
				listeOffreCarte.add(
						new OffreCarte(rs.getInt("idoffrecarte"), rs.getString("type_carte"), rs.getString("description_carte"), rs.getString("photo_carte")));
			}
			
		} catch (Exception ee) {
			ee.printStackTrace();
		} finally {
			// fermeture du rs, du preparedStatement et de la connexion
			try {
				if (rs != null)
					rs.close();
			} catch (Exception ignore) {
			}
			try {
				if (ps != null)
					ps.close();
			} catch (Exception ignore) {
			}
			try {
				if (con != null)
					con.close();
			} catch (Exception ignore) {
			}
		}
		return listeOffreCarte;

	}
	
	public ArrayList<OffreCarte> rechercheOffreCarte(String mot_cle) {

		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList<OffreCarte> listeOffreCarte = new ArrayList<OffreCarte>();

		// connexion à la base de données
		try {

			con = DriverManager.getConnection(URL, UTILISATEUR, MOTDEPASSE);
			ps = con.prepareStatement("SELECT * FROM offrecarte WHERE type_carte LIKE ? OR description_carte LIKE ?");
			ps.setString(1, '%'+mot_cle+'%');
			ps.setString(2, '%'+mot_cle+'%');
			
			// on exécute la requête
			rs = ps.executeQuery();
			// on parcourt les lignes du résultat
			while (rs.next()) {
				listeOffreCarte.add(
						new OffreCarte(rs.getInt("idoffrecarte"), rs.getString("type_carte"), rs.getString("description_carte"), rs.getString("photo_carte")));
			}
			
		} catch (Exception ee) {
			ee.printStackTrace();
		} finally {
			// fermeture du rs, du preparedStatement et de la connexion
			try {
				if (rs != null)
					rs.close();
			} catch (Exception ignore) {
			}
			try {
				if (ps != null)
					ps.close();
			} catch (Exception ignore) {
			}
			try {
				if (con != null)
					con.close();
			} catch (Exception ignore) {
			}
		}
		return listeOffreCarte;

	}
	
	public boolean suppressionOffreCarte(int idOffreCarte) {
		Connection con = null;
		PreparedStatement ps = null;

		try {
			con = DriverManager.getConnection(URL, UTILISATEUR, MOTDEPASSE);
			ps = con.prepareStatement("DELETE FROM offrecarte WHERE idoffrecarte=?");
			ps.setInt(1, idOffreCarte);
			ps.executeUpdate();
			
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			try {
				if (ps != null) {
					ps.close();
				}
			} catch (Exception ignore) {
			}
			try {
				if (con != null) {
					con.close();
				}
			} catch (Exception ignore) {
			}
		}
		return true;
	}
}