package com.octest.models;
import java.sql.*;
import java.util.*;
import com.octest.dto.*;


public class EpargneDAO {
	private static String URL = "jdbc:mysql://localhost/projets8?useSSL=true&serverTimezone=Europe/Paris";
	private static String UTILISATEUR = "root";
	private static String MOTDEPASSE = "mddc1260";
	
	public EpargneDAO() {
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	public boolean compteOK(int idCompte) {
		int idEpargne = 0;
		Connection con = null;
		PreparedStatement ps = null;
        ResultSet rs = null;
        try {
			con = DriverManager.getConnection(URL, UTILISATEUR, MOTDEPASSE);
			ps = con.prepareStatement("SELECT idEpargne FROM epargne WHERE epa_idCompte = ?");
			ps.setInt(1, idCompte);
			rs=ps.executeQuery();
			
			while(rs.next()) {
			    idEpargne=rs.getInt(1);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			try {
				if (ps != null) {
					ps.close();
				}
			} catch (Exception ignore) {
			}
			try {
				if (con != null) {
					con.close();
				}
			} catch (Exception ignore) {
			}
			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception ignore) {
			}
		}
		return true;
	}
	
	public boolean creerCompte(int idCompte, String type, float solde, float taux){
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = DriverManager.getConnection(URL, UTILISATEUR, MOTDEPASSE);
			ps = con.prepareStatement("INSERT INTO epargne (epa_idCompte, epa_type, epa_solde, epa_taux) VALUES (?, ?, ?, ?)");
			ps.setInt(1, idCompte);
			ps.setString(2, type);
			ps.setFloat(3, solde);
			ps.setFloat(4, taux);
			ps.executeUpdate();
			
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			try {
				if (ps != null) {
					ps.close();
				}
			} catch (Exception ignore) {
			}
			try {
				if (con != null) {
					con.close();
				}
			} catch (Exception ignore) {
			}
		}
		return true;
	}
	
	public boolean alimCompte(int idEpargne, float solde) {
		PreparedStatement ps = null;
		Connection con = null;
		
		try {
			    con = DriverManager.getConnection(URL, UTILISATEUR, MOTDEPASSE);
				ps = con.prepareStatement("Update epargne SET epa_solde = ? WHERE idEpargne = ?");
				ps.setFloat(1, solde);
				ps.setInt(2, idEpargne);
				ps.executeUpdate();
				
                
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			try {
				if (ps != null) {
					ps.close();
				}
			} catch (Exception ignore) {
			}
			try {
				if (con != null) {
					con.close();
				}
			} catch (Exception ignore) {
			}
		}
		return true;
	}
	
	public Epargne getEpargne(int idCompte) {
		Epargne epa = null;
		Connection con = null;
		PreparedStatement ps = null;
        ResultSet rs = null;
        try {
			con = DriverManager.getConnection(URL, UTILISATEUR, MOTDEPASSE);
			ps = con.prepareStatement("SELECT idEpargne, epa_idCompte, epa_type, epa_solde, epa_taux FROM epargne WHERE epa_idCompte = ?");
			ps.setInt(1, idCompte);
			rs=ps.executeQuery();
			
			while(rs.next()) {
				epa = new Epargne(rs.getInt(1), rs.getInt(2), rs.getString(3), rs.getFloat(4), rs.getFloat(5));
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (ps != null) {
					ps.close();
				}
			} catch (Exception ignore) {
			}
			try {
				if (con != null) {
					con.close();
				}
			} catch (Exception ignore) {
			}
			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception ignore) {
			}
		}
		return epa;
	}

}
