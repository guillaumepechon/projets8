package com.octest.models;

import java.io.*;
import java.util.*;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.octest.dto.Cac;

public class ReadExcel {

	public List<Cac> getListeCac() throws IOException{
		File ExcelFile = new File("C:\\Users\\guill\\git\\projets8\\WebContent\\file\\cac40.xlsx");
		FileInputStream Fis = new FileInputStream(ExcelFile);
		XSSFWorkbook Workbook = new XSSFWorkbook(Fis);
		XSSFSheet Sheet = Workbook.getSheetAt(0);
		Iterator<Row> RowIt = Sheet.iterator();
		List<Cac> List = new ArrayList<Cac>();
		
		try {
			
			while(RowIt.hasNext()) {
				
				Row row = RowIt.next();
				List<String> cac = new ArrayList<String>();
				Iterator<Cell> CellIterator = row.cellIterator();

				while (CellIterator.hasNext()) {
					Cell cell = CellIterator.next();
					cac.add(cell.toString());

				}

				String nom = cac.get(0);
				String ouverture = cac.get(1);
				String haut = cac.get(2);
				String bas = cac.get(3);
				String volume = cac.get(4);
				String veille = cac.get(5);
				String dernier = cac.get(6);
				String var= cac.get(7);
				
				List.add(new Cac(nom,ouverture,haut,bas,volume,veille,dernier,var));
				
			}
		} finally {
			Workbook.close();
			Fis.close();
		}
		
		List.remove(0);
		return List;
	}
    
}
