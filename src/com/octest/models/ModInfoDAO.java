package com.octest.models;
import java.sql.Connection;
import java.util.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class ModInfoDAO {
	
	private static String URL = "jdbc:mysql://localhost/projets8?useSSL=true&serverTimezone=Europe/Paris";
	private static String UTILISATEUR = "root";
	private static String MOTDEPASSE = "mddc1260";
	

	public ModInfoDAO() {
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	
	public boolean modInfo(String nom, String prenom, String mail, String mot_de_passe, String date_naissance, String lieu_naissance, String adresse, String ville, int idClient) {
		Connection con = null;
		PreparedStatement ps = null;
		java.sql.Date dob = java.sql.Date.valueOf(date_naissance);

		try {
			con = DriverManager.getConnection(URL, UTILISATEUR, MOTDEPASSE);
			ps = con.prepareStatement("UPDATE client SET cli_nom = ?, cli_prenom = ? , cli_email = ? , cli_mdp = ? , cli_date = ? , cli_lieu_naissance = ? , cli_adresse = ? , cli_ville = ? WHERE idClient = ? ");
			ps.setString(1, nom);
			ps.setString(2, prenom);
			ps.setString(3, mail);
			ps.setString(4, mot_de_passe);
			ps.setDate(5, dob);
			ps.setString(6, lieu_naissance);
			ps.setString(7, adresse);
			ps.setString(8, ville);
			ps.setInt(9, idClient);
			ps.executeUpdate();
			
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			try {
				if (ps != null) {
					ps.close();
				}
			} catch (Exception ignore) {
			}
			try {
				if (con != null) {
					con.close();
				}
			} catch (Exception ignore) {
			}
		}
		return true;
	}
}