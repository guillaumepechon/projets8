package com.octest.models;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import com.octest.dto.Compte;
import com.octest.dto.Transactions;

public class HistTransactionsDAO {
	
	private static String URL = "jdbc:mysql://localhost/projets8?useSSL=false&serverTimezone=Europe/Paris";
	private static String UTILISATEUR = "root";
	private static String MOTDEPASSE = "mddc1260";
	

	public HistTransactionsDAO() {
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}
	/*
	public void setIdClient(int idClient) {
		System.out.println(idClient);
		idClient2=idClient;
		System.out.println(idClient2);
	}
	*/
	/*
	public int ClientToCompte(int idClient2) {
		
		int idCompte=0;
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		System.out.println("Client"+idClient2);

		// connexion à la base de données
		try {
			con = DriverManager.getConnection(URL, UTILISATEUR, MOTDEPASSE);
			ps = con.prepareStatement("SELECT * FROM compte WHERE cpt_idClient=?");
			ps.setInt(1, idClient2);
			// on exécute la requête
			rs = ps.executeQuery();
			
			Compte compte = new Compte(rs.getInt("idCompte"),rs.getInt("cpt_idClient"),rs.getInt("cpt_type"),rs.getInt("cpt_solde"));
			idCompte=rs.getInt("idCompte"); //pblm
			System.out.println(idCompte+"test");
			
		} catch (Exception ee) {
			ee.printStackTrace();
		} finally {
			// fermeture du rs, du preparedStatement et de la connexion
			try {
				if (rs != null)
					rs.close();
			} catch (Exception ignore) {
			}
			try {
				if (ps != null)
					ps.close();
			} catch (Exception ignore) {
			}
			try {
				if (con != null)
					con.close();
			} catch (Exception ignore) {
			}
		}
		return idCompte;

	}
	
	public ArrayList<Transactions> RecupererHistTransactions(int idCompte) {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList<Transactions> histTransaction = new ArrayList<Transactions>();
		System.out.println("Compte"+idCompte);

		// connexion à la base de données
		try {
			con = DriverManager.getConnection(URL, UTILISATEUR, MOTDEPASSE);
			ps = con.prepareStatement("SELECT * FROM transaction WHERE trans_idCompte=?");
			ps.setInt(1, idCompte);

			// on exécute la requête
			rs = ps.executeQuery();
			// on parcourt les lignes du résultat
			while (rs.next()) {
				histTransaction.add(new Transactions(rs.getInt("idTrans"),rs.getInt("trans_idCompte"),rs.getInt("trans_idCompte_emetteur"),rs.getInt("trans_idCompte_recepteur"), rs.getDate("trans_date"), rs.getString("trans_type"), rs.getFloat("trans_montant"), rs.getString("trans_motif")));
				System.out.println(histTransaction);
			}
			
		} catch (Exception ee) {
			ee.printStackTrace();
		} finally {
			// fermeture du rs, du preparedStatement et de la connexion
			try {
				if (rs != null)
					rs.close();
			} catch (Exception ignore) {
			}
			try {
				if (ps != null)
					ps.close();
			} catch (Exception ignore) {
			}
			try {
				if (con != null)
					con.close();
			} catch (Exception ignore) {
			}
		}
		return histTransaction;

	}*/
	/*//TEST1
	public int RecupererHistTransactions(int idClient2) {
		
		int idCompte=0;
		ArrayList<Transactions> histTransaction = new ArrayList<Transactions>();
		
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		System.out.println("Compte"+idCompte);

		// connexion à la base de données
		try {
			con = DriverManager.getConnection(URL, UTILISATEUR, MOTDEPASSE);
			ps = con.prepareStatement("SELECT * FROM compte WHERE cpt_idClient=?");
			ps.setInt(1, idClient2);

			// on exécute la requête
			rs = ps.executeQuery();
			
			idCompte=rs.getInt("idCompte");
			System.out.println(idCompte+"test");
			
			
			// fermeture du rs, du preparedStatement et de la connexion
			try {
				if (rs != null)
					rs.close();
			} catch (Exception ignore) {
			}
			try {
				if (ps != null)
					ps.close();
			} catch (Exception ignore) {
			}
			try {
				if (con != null)
					con.close();
			} catch (Exception ignore) {
			}
			
			
			ps = con.prepareStatement("SELECT * FROM transaction WHERE trans_idCompte=?");
			ps.setInt(1, idCompte);
			rs = ps.executeQuery();
			// on parcourt les lignes du résultat
			while (rs.next()) {
				histTransaction.add(new Transactions(rs.getInt("idTrans"),rs.getInt("trans_idCompte"),rs.getInt("trans_idCompte_emetteur"),rs.getInt("trans_idCompte_recepteur"), rs.getDate("trans_date"), rs.getString("trans_type"), rs.getFloat("trans_montant"), rs.getString("trans_motif")));
				System.out.println(histTransaction);
			}
			
		} catch (Exception ee) {
			ee.printStackTrace();
		} finally {
			// fermeture du rs, du preparedStatement et de la connexion
			try {
				if (rs != null)
					rs.close();
			} catch (Exception ignore) {
			}
			try {
				if (ps != null)
					ps.close();
			} catch (Exception ignore) {
			}
			try {
				if (con != null)
					con.close();
			} catch (Exception ignore) {
			}
		}
		return idCompte;

	}
}
	*/
	
	public Compte getCompte(int idClient) {
		System.out.println("getCompte Debut");
		Compte cpt = null;
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		try {
			con = DriverManager.getConnection(URL, UTILISATEUR, MOTDEPASSE);
			ps = con.prepareStatement("SELECT idCompte, cpt_idClient, cpt_offre, cpt_solde, cpt_iban FROM compte WHERE cpt_idClient = ? ");
			ps.setInt(1, idClient);
			
			rs = ps.executeQuery();
			if (rs.next()) {
				cpt = new Compte(rs.getInt(1), rs.getInt(2), rs.getString(3), rs.getFloat(4), rs.getString(5));
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (ps != null) {
					ps.close();
				}
			} catch (Exception ignore) {
			}
			try {
				if (con != null) {
					con.close();
				}
			} catch (Exception ignore) {
			}
			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception ignore) {
			}
		}
		System.out.println("getCompte Fin");
		return cpt;
	}
	
	
	public ArrayList<Transactions> RecupererHistTransactions(int idClient) {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList<Transactions> histTransaction = new ArrayList<Transactions>();
		
		System.out.println("RecupererHistTransactions Debut");
		
		Compte compte=getCompte(idClient);
		System.out.println(compte.getIdCompte());
		int id_compte=compte.getIdCompte();

		System.out.println("RecupererHistTransactions Fin");
		// connexion à la base de données
		try {
			con = DriverManager.getConnection(URL, UTILISATEUR, MOTDEPASSE);
			ps = con.prepareStatement("SELECT * FROM transaction WHERE trans_idCompte=?");
			ps.setInt(1, id_compte);

			// on exécute la requête
			rs = ps.executeQuery();
			// on parcourt les lignes du résultat
			while (rs.next()) {
				histTransaction.add(new Transactions(rs.getInt("idTrans"),rs.getInt("trans_idCompte"),rs.getInt("trans_idCompte_emetteur"),rs.getInt("trans_idCompte_recepteur"), rs.getDate("trans_date"), rs.getString("trans_type"), rs.getFloat("trans_montant"), rs.getString("trans_motif")));
				System.out.println(histTransaction);
			}
			
		} catch (Exception ee) {
			ee.printStackTrace();
		} finally {
			// fermeture du rs, du preparedStatement et de la connexion
			try {
				if (rs != null)
					rs.close();
			} catch (Exception ignore) {
			}
			try {
				if (ps != null)
					ps.close();
			} catch (Exception ignore) {
			}
			try {
				if (con != null)
					con.close();
			} catch (Exception ignore) {
			}
		}
		return histTransaction;

	}
}

	