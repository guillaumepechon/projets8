package com.octest.models;
import java.sql.Connection;
import java.util.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class InscriptionDAO {
	
	private static String URL = "jdbc:mysql://localhost/projets8?useSSL=true&serverTimezone=Europe/Paris";
	private static String UTILISATEUR = "root";
	private static String MOTDEPASSE = "mddc1260";
	

	public InscriptionDAO() {
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	
	public boolean ajoutInscription(String civi, String nom, String prenom, String mail, String mot_de_passe, String date_naissance, String ville_naissance, String pays_naissance, String nation, String adresse, String ville, String pays, String tel) {
		Connection con = null;
		PreparedStatement ps = null;
		java.sql.Date dob = java.sql.Date.valueOf(date_naissance);

		try {
			con = DriverManager.getConnection(URL, UTILISATEUR, MOTDEPASSE);
			ps = con.prepareStatement("INSERT INTO client (cli_civi, cli_nom, cli_prenom, cli_email, cli_mdp, cli_date, cli_ville_naissance, cli_pays_naissance, cli_nation, cli_adresse, cli_ville, cli_pays, cli_tel) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)");
			ps.setString(1, civi);
			ps.setString(2, nom);
			ps.setString(3, prenom);
			ps.setString(4, mail);
			ps.setString(5, mot_de_passe);
			ps.setDate(6, dob);
			ps.setString(7, ville_naissance);
			ps.setString(8, pays_naissance);
			ps.setString(9, nation);
			ps.setString(10, adresse);
			ps.setString(11, ville);
			ps.setString(12, pays);
			ps.setString(13, tel);
			ps.executeUpdate();
			
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			try {
				if (ps != null) {
					ps.close();
				}
			} catch (Exception ignore) {
			}
			try {
				if (con != null) {
					con.close();
				}
			} catch (Exception ignore) {
			}
		}
		return true;
	}
	
	public int getId(String email, String mdp) {
		Connection con = null;
		PreparedStatement ps = null;
        ResultSet rs = null;
        int id = 0;
        
		try {
			con = DriverManager.getConnection(URL, UTILISATEUR, MOTDEPASSE);
			ps = con.prepareStatement("SELECT idClient FROM client WHERE cli_email = ? AND cli_mdp = ?");
			ps.setString(1, email);
			ps.setString(2, mdp);
			rs=ps.executeQuery();
			
			
			while(rs.next()) {
				id = rs.getInt(1);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (ps != null) {
					ps.close();
				}
			} catch (Exception ignore) {
			}
			try {
				if (con != null) {
					con.close();
				}
			} catch (Exception ignore) {
			}
		}
		return id;
	}
	public boolean creerCompte(int idClient, String offre, Float solde) {
		Connection con = null;
		PreparedStatement ps = null;

		try {
			con = DriverManager.getConnection(URL, UTILISATEUR, MOTDEPASSE);
			ps = con.prepareStatement("INSERT INTO compte (cpt_idClient, cpt_offre, cpt_solde) VALUES (?,?,?)");
			ps.setInt(1, idClient);
			ps.setString(2, offre);
			ps.setFloat(3, solde);
			ps.executeUpdate();
			
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			try {
				if (ps != null) {
					ps.close();
				}
			} catch (Exception ignore) {
			}
			try {
				if (con != null) {
					con.close();
				}
			} catch (Exception ignore) {
			}
		}
		return true;
	}
}