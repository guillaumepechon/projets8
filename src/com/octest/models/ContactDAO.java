package com.octest.models;

import java.sql.Connection;
import java.util.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class ContactDAO {
	
	private static String URL = "jdbc:mysql://localhost/projets8?useSSL=true&serverTimezone=Europe/Paris";
	private static String UTILISATEUR = "root";
	private static String MOTDEPASSE = "mddc1260";
	

	public ContactDAO() {
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	
	public boolean ajoutFormulaireContact(String nom, String prenom, String mail, String message) {
		Connection con = null;
		PreparedStatement ps = null;
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		Date date = new Date();
		String dateF = df.format(date.getTime());
		java.sql.Date datefin= java.sql.Date.valueOf(dateF);
		

		try {
			con = DriverManager.getConnection(URL, UTILISATEUR, MOTDEPASSE);
			ps = con.prepareStatement("INSERT INTO contact (con_nom,con_prenom,con_email,con_date,con_message) VALUES (?,?,?,?,?)");
			ps.setString(1, nom);
			ps.setString(2, prenom);
			ps.setString(3, mail);
			ps.setDate(4, datefin);
			ps.setString(5, message);
			ps.executeUpdate();
			
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			try {
				if (ps != null) {
					ps.close();
				}
			} catch (Exception ignore) {
			}
			try {
				if (con != null) {
					con.close();
				}
			} catch (Exception ignore) {
			}
		}
		return true;
	}
}