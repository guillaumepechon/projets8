package com.octest.models;

import java.sql.*;
import java.util.*;
import com.octest.dto.*;

public class CompteDAO {
	
	static final String JDBC_DRIVER = "com.mysql.cj.jdbc.Driver";  
    static final String URL = "jdbc:mysql://localhost/projets8?serverTimezone=Europe/Paris";
    
    static final String UTILISATEUR = "root";
    static final String MOTDEPASSE = "mddc1260";
	

	public CompteDAO() {
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	
	public boolean ajouterCompte(String nom, String prenom, String mail, String mot_de_passe, String type_compte) {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		int idClient;
		
		try {
			con = DriverManager.getConnection(URL, UTILISATEUR, MOTDEPASSE);
			ps = con.prepareStatement("SELECT idClient FROM client WHERE cli_nom = ? AND cli_prenom = ? AND cli_email = ? AND cli_mdp = ?");
			ps.setString(1, nom);
			ps.setString(2, prenom);
			ps.setString(3, mail);
			ps.setString(4, mot_de_passe);
			
			rs = ps.executeQuery();
			// passe à la première (et unique) ligne retournée
			if (rs.next()) {
				idClient = rs.getInt("idclient");
			}else {
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			try {
				if (ps != null) {
					ps.close();
				}
			} catch (Exception ignore) {
			}
			try {
				if (con != null) {
					con.close();
				}
			} catch (Exception ignore) {
			}
		}
		try {
			con = DriverManager.getConnection(URL, UTILISATEUR, MOTDEPASSE);
			ps = con.prepareStatement(
					"INSERT INTO compte (cpt_idClient,cpt_type) VALUES (?,?)");
			ps.setInt(1, idClient);
			ps.setString(2, type_compte);
			ps.executeUpdate();

		} catch (Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			try {
				if (ps != null) {
					ps.close();
				}
			} catch (Exception ignore) {
			}
			try {
				if (con != null) {
					con.close();
				}
			} catch (Exception ignore) {
			}
		}
		return true;
	}
	
	public int nombreCompte(String nom, String prenom, String mail, String mot_de_passe) {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		int idClient = 0;
		int nombreCompte = 0;
		
		try {
			con = DriverManager.getConnection(URL, UTILISATEUR, MOTDEPASSE);
			ps = con.prepareStatement("SELECT idClient FROM client WHERE cli_nom = ? AND cli_prenom = ? AND cli_email = ? AND cli_mdp = ?");
			ps.setString(1, nom);
			ps.setString(2, prenom);
			ps.setString(3, mail);
			ps.setString(4, mot_de_passe);
			
			rs = ps.executeQuery();
			// passe à la première (et unique) ligne retournée
			if (rs.next()) {
				idClient = rs.getInt("idclient");
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (ps != null) {
					ps.close();
				}
			} catch (Exception ignore) {
			}
			try {
				if (con != null) {
					con.close();
				}
			} catch (Exception ignore) {
			}
		}
		try {
			con = DriverManager.getConnection(URL, UTILISATEUR, MOTDEPASSE);
			ps = con.prepareStatement(
					"SELECT COUNT(*) FROM compte WHERE cpt_idClient = ?");
			ps.setInt(1, idClient);
			rs = ps.executeQuery();
			// passe à la première (et unique) ligne retournée
			if (rs.next()) {
				nombreCompte = rs.getInt("COUNT(*)");
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (ps != null) {
					ps.close();
				}
			} catch (Exception ignore) {
			}
			try {
				if (con != null) {
					con.close();
				}
			} catch (Exception ignore) {
			}
		}
		return nombreCompte;
	}
	
	public String typeCompte(String nom, String prenom, String mail, String mot_de_passe) {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		int idClient = 0;
		String typeCompte = null;
		
		try {
			con = DriverManager.getConnection(URL, UTILISATEUR, MOTDEPASSE);
			ps = con.prepareStatement("SELECT idClient FROM client WHERE cli_nom = ? AND cli_prenom = ? AND cli_email = ? AND cli_mdp = ?");
			ps.setString(1, nom);
			ps.setString(2, prenom);
			ps.setString(3, mail);
			ps.setString(4, mot_de_passe);
			
			rs = ps.executeQuery();
			// passe à la première (et unique) ligne retournée
			if (rs.next()) {
				idClient = rs.getInt("idclient");
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (ps != null) {
					ps.close();
				}
			} catch (Exception ignore) {
			}
			try {
				if (con != null) {
					con.close();
				}
			} catch (Exception ignore) {
			}
		}
		try {
			con = DriverManager.getConnection(URL, UTILISATEUR, MOTDEPASSE);
			ps = con.prepareStatement(
					"SELECT cpt_type FROM compte WHERE cpt_idClient = ? IS NOT NULL");
			ps.setInt(1, idClient);
			rs = ps.executeQuery();
			// passe à la première (et unique) ligne retournée
			if (rs.next()) {
				typeCompte = rs.getString("cpt_type");
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (ps != null) {
					ps.close();
				}
			} catch (Exception ignore) {
			}
			try {
				if (con != null) {
					con.close();
				}
			} catch (Exception ignore) {
			}
		}
		return typeCompte;
	}
	
public boolean alimCompte (int idClient, float solde)
	
	{
		PreparedStatement ps = null;
		Connection con = null;
		ResultSet rs = null;
		
		try {
			    con = DriverManager.getConnection(URL, UTILISATEUR, MOTDEPASSE);
				ps = con.prepareStatement("Update compte SET cpt_solde = ? WHERE cpt_idClient = ?");
				ps.setFloat(1, solde);
				ps.setInt(2, idClient);
				ps.executeUpdate();
				
                
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			try {
				if (ps != null) {
					ps.close();
				}
			} catch (Exception ignore) {
			}
			try {
				if (con != null) {
					con.close();
				}
			} catch (Exception ignore) {
			}
		}
		return true;
	}

public boolean virIntCompte (int idClient, String emet, String rec, float cash, String motif)

{
	PreparedStatement request = null;
	ResultSet rs = null;
	java.sql.Connection con = null;
	
	try {
		con = DriverManager.getConnection(URL, UTILISATEUR, MOTDEPASSE);
			request = con.prepareStatement("SELECT idCompte FROM compte WHERE cpt_idClient=? AND cpt_type=?");
			request.setInt(1, idClient);
			request.setString(2, emet);
			rs = request.executeQuery();
			System.out.println(idClient);
			
			if(rs.next())
			{
				int idCompte1 = rs.getInt("idCompte");
				request = con.prepareStatement("SELECT idCompte FROM compte WHERE cpt_idClient=? AND cpt_type=?");
				request.setInt(1, idClient);
				request.setString(2, rec);
				rs = request.executeQuery();
				System.out.println(idCompte1);
				
				if(rs.next())
				{
					int idCompte2 = rs.getInt("idCompte");
					request = con.prepareStatement("SELECT cpt_solde FROM compte WHERE idCompte = ?");
					request.setInt(1, idCompte1);
					rs = request.executeQuery();
					System.out.println(idCompte2);
					
					if(rs.next())
					{
						float emetsolde =  rs.getFloat("cpt_solde");
						request = con.prepareStatement("SELECT cpt_solde FROM compte WHERE idCompte = ?");
						request.setInt(1, idCompte2);
						rs = request.executeQuery();
						System.out.println(emetsolde);
						
						if(rs.next())
						{
							float recsolde =  rs.getFloat("cpt_solde");
							System.out.println(recsolde);
							emetsolde= emetsolde-cash;
							recsolde= recsolde+cash;
							System.out.println(emetsolde);
							System.out.println(recsolde);
							request = con.prepareStatement("UPDATE compte SET cpt_solde = ? WHERE idCompte = ?" );
							request.setFloat(1,emetsolde);
							request.setInt(2,idCompte1);
							request.executeUpdate();	
							request = con.prepareStatement("UPDATE compte SET cpt_solde = ? WHERE idCompte = ?" );
							request.setFloat(1,recsolde);
							request.setInt(2,idCompte2);
							request.executeUpdate();
						
							String type = "Virement interne";
							request = con.prepareStatement("INSERT INTO transaction (trans_idCompte, trans_idCompte_emetteur, trans_idCompte_recepteur, trans_date, trans_type, trans_montant, trans_motif)" +
									"VALUES (?,?,?,Now(),?,?,?)" );
							request.setInt(1,idCompte1);
							request.setInt(2,idCompte1);
							request.setInt(3,idCompte2);
							request.setString(4,type);
							request.setFloat(5,cash);
							request.setString(6,motif);
							request.executeUpdate();
							System.out.println(rs);
							return true;
						
							
						
					}
				}
			}
            
		}
	   
	
		con.close();
		return false;
	
	}
	catch(SQLException e)
	{
		e.printStackTrace();
		return false;
	}
	
	
	
}



public float getSolde(int idClient, String type) {
	Connection con = null;
	PreparedStatement ps = null;
	ResultSet rs = null;
	float retour =0;
	
	try {
		con = DriverManager.getConnection(URL, UTILISATEUR, MOTDEPASSE);
		ps = con.prepareStatement("SELECT cpt_solde FROM compte WHERE cpt_idClient=? ");
		ps.setInt(1, idClient);
		ps.setString(2, type);
		
		rs = ps.executeQuery();
		if (rs.next()) {
			retour = rs.getInt("cpt_solde");
		}
	} catch (Exception e) {
		e.printStackTrace();
	} finally {
		try {
			if (ps != null) {
				ps.close();
			}
		} catch (Exception ignore) {
		}
		try {
			if (con != null) {
				con.close();
			}
		} catch (Exception ignore) {
		}
	}
	return retour;
}

public Compte getCompte(int idClient) {
	Compte cpt = null;
	Connection con = null;
	PreparedStatement ps = null;
	ResultSet rs = null;
	
	try {
		con = DriverManager.getConnection(URL, UTILISATEUR, MOTDEPASSE);
		ps = con.prepareStatement("SELECT idCompte, cpt_idClient, cpt_offre, cpt_solde, cpt_iban FROM compte WHERE cpt_idClient = ? ");
		ps.setInt(1, idClient);
		
		rs = ps.executeQuery();
		if (rs.next()) {
			cpt = new Compte(rs.getInt(1), rs.getInt(2), rs.getString(3), rs.getFloat(4), rs.getString(5));
		}
	} catch (Exception e) {
		e.printStackTrace();
	} finally {
		try {
			if (ps != null) {
				ps.close();
			}
		} catch (Exception ignore) {
		}
		try {
			if (con != null) {
				con.close();
			}
		} catch (Exception ignore) {
		}
		try {
			if (rs != null) {
				rs.close();
			}
		} catch (Exception ignore) {
		}
	}
	return cpt;
}

public static void main(String[] args){
	//CompteDAO myDao= new CompteDAO();
	//myDao.virIntCompte("mm@gmail.com", "Compte Courant","Compte Epargne", "3/04/2019", 100, "Besoin d'argent");
}
}
