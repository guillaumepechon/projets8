package com.octest.models;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import com.octest.dto.Client;


public class ConnexionDAO {
	
	private static String URL = "jdbc:mysql://localhost/projets8?useSSL=true&serverTimezone=Europe/Paris";
	private static String UTILISATEUR = "root";
	private static String MOTDEPASSE = "mddc1260";
	

	public ConnexionDAO() {
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	
	public String getNom(String email, String mdp) {
		Connection con = null;
		PreparedStatement ps = null;
        ResultSet rs = null;
        String nom = null;
        
		try {
			con = DriverManager.getConnection(URL, UTILISATEUR, MOTDEPASSE);
			ps = con.prepareStatement("SELECT cli_nom FROM client WHERE cli_email = ? AND cli_mdp = ?");
			ps.setString(1, email);
			ps.setString(2, mdp);
			rs=ps.executeQuery();
			
			
			while(rs.next()) {
				nom = rs.getString(1);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (ps != null) {
					ps.close();
				}
			} catch (Exception ignore) {
			}
			try {
				if (con != null) {
					con.close();
				}
			} catch (Exception ignore) {
			}
		}
		return nom;
	}
	
	public Client getClient(String email, String mdp) {
		Client cli = null;
		Connection con = null;
		PreparedStatement ps = null;
        ResultSet rs = null;
        try {
			con = DriverManager.getConnection(URL, UTILISATEUR, MOTDEPASSE);
			ps = con.prepareStatement("SELECT idClient, cli_civi, cli_nom, cli_prenom, cli_email, cli_mdp, cli_ville_naissance, cli_pays_naissance, cli_nation, cli_adresse, cli_ville, cli_pays, cli_tel FROM client WHERE cli_email = ? AND cli_mdp = ?");
			ps.setString(1, email);
			ps.setString(2, mdp);
			rs=ps.executeQuery();
			
			while(rs.next()) {
				cli = new Client(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6), rs.getString(7), rs.getString(8), rs.getString(9), rs.getString(10), rs.getString(11), rs.getString(12), rs.getString(13));
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (ps != null) {
					ps.close();
				}
			} catch (Exception ignore) {
			}
			try {
				if (con != null) {
					con.close();
				}
			} catch (Exception ignore) {
			}
			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception ignore) {
			}
		}
		return cli;
	}
}