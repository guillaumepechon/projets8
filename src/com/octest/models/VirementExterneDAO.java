package com.octest.models;

import java.sql.Connection;
import java.util.Date;

import org.iban4j.IbanFormat;
import org.iban4j.IbanFormatException;
import org.iban4j.IbanUtil;
import org.iban4j.InvalidCheckDigitException;
import org.iban4j.UnsupportedCountryException;

import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import com.octest.dto.Compte;

public class VirementExterneDAO {
	
	private static String URL = "jdbc:mysql://localhost/projets8?useSSL=false&serverTimezone=Europe/Paris";
	private static String UTILISATEUR = "root";
	private static String MOTDEPASSE = "mddc1260";
	

	public VirementExterneDAO() {
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

	public Compte getCompte(int idClient) {
		Compte cpt = null;
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		try {
			con = DriverManager.getConnection(URL, UTILISATEUR, MOTDEPASSE);
			ps = con.prepareStatement("SELECT idCompte, cpt_idClient, cpt_offre, cpt_solde, cpt_iban FROM compte WHERE cpt_idClient = ? ");
			ps.setInt(1, idClient);
			
			rs = ps.executeQuery();
			if (rs.next()) {
				cpt = new Compte(rs.getInt(1), rs.getInt(2), rs.getString(3), rs.getFloat(4), rs.getString(5));
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (ps != null) {
					ps.close();
				}
			} catch (Exception ignore) {
			}
			try {
				if (con != null) {
					con.close();
				}
			} catch (Exception ignore) {
			}
			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception ignore) {
			}
		}
		return cpt;
	}
	
	public Compte getCompteIban(String iban) {
		Compte cpt = null;
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		try {
			con = DriverManager.getConnection(URL, UTILISATEUR, MOTDEPASSE);
			ps = con.prepareStatement("SELECT idCompte, cpt_idClient, cpt_offre, cpt_solde, cpt_iban FROM compte WHERE cpt_iban = ? ");
			ps.setString(1, iban);
			
			rs = ps.executeQuery();
			if (rs.next()) {
				cpt = new Compte(rs.getInt(1), rs.getInt(2), rs.getString(3), rs.getFloat(4), rs.getString(5));
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (ps != null) {
					ps.close();
				}
			} catch (Exception ignore) {
			}
			try {
				if (con != null) {
					con.close();
				}
			} catch (Exception ignore) {
			}
			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception ignore) {
			}
		}
		return cpt;
	}
	
	public boolean isValidIban(String iban) {
	    try {
	    	IbanUtil.validate(iban);
	    }catch (IbanFormatException | InvalidCheckDigitException | UnsupportedCountryException e) {
	        return false;
	    }
	    return true;
	}
	
	
	public boolean virementExterne(int idClient,String ibanRecepteur, float montantTransaction,String motif) {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		Date date = new Date();
		String dateF = df.format(date.getTime());
		java.sql.Date datefin= java.sql.Date.valueOf(dateF);
		
		Compte compteEmetteur=getCompte(idClient);
		int id_compteEmetteur=compteEmetteur.getIdCompte();
		float solde_compteEmetteur=compteEmetteur.getSolde();
		float nouveausolde_compteEmetteur=solde_compteEmetteur - montantTransaction;
		
		isValidIban(ibanRecepteur);
	        
	    Compte compteRecepteur=getCompteIban(ibanRecepteur);
	    int id_compteRecepteur=compteRecepteur.getIdCompte();
	    float solde_compteRecepteur=compteRecepteur.getSolde();
	    float nouveausolde_compteRecepteur=solde_compteRecepteur + montantTransaction;
	        
	    // connexion à la base de données
		try {
			con = DriverManager.getConnection(URL, UTILISATEUR, MOTDEPASSE);
			ps = con.prepareStatement("UPDATE compte SET cpt_solde=? WHERE idCompte=?");
			ps.setFloat(1, nouveausolde_compteEmetteur);
			ps.setInt(2, id_compteEmetteur);
			ps.executeUpdate();
				
		} catch (Exception ee) {
				ee.printStackTrace();
				return false;
		}finally {
			// fermeture du rs, du preparedStatement et de la connexion
			try {
				if (rs != null)
					rs.close();
			} catch (Exception ignore) {
			}
			try {
				if (ps != null)
					ps.close();
			} catch (Exception ignore) {
			}
			try {
				if (con != null)
					con.close();
			} catch (Exception ignore) {
			}
		}
		try {
			con = DriverManager.getConnection(URL, UTILISATEUR, MOTDEPASSE);
			ps = con.prepareStatement("UPDATE compte SET cpt_solde=? WHERE idCompte=?");
			ps.setFloat(1, nouveausolde_compteRecepteur);
			ps.setInt(2, id_compteRecepteur);
			ps.executeUpdate();
				
		} catch (Exception ee) {
			ee.printStackTrace();
			return false;
		}finally {
			// fermeture du rs, du preparedStatement et de la connexion
			try {
				if (rs != null)
					rs.close();
			} catch (Exception ignore) {
			}
			try {
				if (ps != null)
					ps.close();
			} catch (Exception ignore) {
			}
			try {
				if (con != null)
					con.close();
			} catch (Exception ignore) {
			}
		}
		try {
			con = DriverManager.getConnection(URL, UTILISATEUR, MOTDEPASSE);
			ps = con.prepareStatement("INSERT INTO transaction (trans_idCompte, trans_idCompte_emetteur, trans_idCompte_recepteur, trans_date, trans_type, trans_montant, trans_motif) VALUES (?,?,?,?,?,?,?)");
			ps.setInt(1, id_compteEmetteur);
			ps.setInt(2, id_compteEmetteur);
			ps.setInt(3, id_compteRecepteur);
			ps.setDate(4, datefin);
			ps.setString(5, "Virement externe");
			ps.setFloat(6, montantTransaction);
			ps.setString(7, motif);
			ps.executeUpdate();
				
		} catch (Exception ee) {
			ee.printStackTrace();
			return false;
		}finally {
			// fermeture du rs, du preparedStatement et de la connexion
			try {
				if (rs != null)
					rs.close();
			} catch (Exception ignore) {
			}
			try {
				if (ps != null)
					ps.close();
			} catch (Exception ignore) {
			}
			try {
				if (con != null)
					con.close();
			} catch (Exception ignore) {
			}
		}
	    
		return true;

	}
	
	public static void main(String [] args) {
		VirementExterneDAO mydao = new VirementExterneDAO();
		System.out.println(mydao.isValidIban("FR7630004001160001028026721"));
	}
}

	