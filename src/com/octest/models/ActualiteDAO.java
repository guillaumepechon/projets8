package com.octest.models;

import java.io.InputStream;
import java.sql.Connection;
import java.util.Date;

import javax.servlet.http.Part;

import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import com.octest.dto.Actualite;

public class ActualiteDAO {
	
	private static String URL = "jdbc:mysql://localhost/projets8?useSSL=false&serverTimezone=Europe/Paris&characterEncoding=UTF-8";
	private static String UTILISATEUR = "root";
	private static String MOTDEPASSE = "mddc1260";
	

	public ActualiteDAO() {
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	
	public boolean ajoutActualite(String act_title, String act_contenu, String file, String act_contenu_det) {
		Connection con = null;
		PreparedStatement ps = null;
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		Date date = new Date();
		String dateF = df.format(date.getTime());
		java.sql.Date datefin= java.sql.Date.valueOf(dateF);

		try {
			con = DriverManager.getConnection(URL, UTILISATEUR, MOTDEPASSE);
			ps = con.prepareStatement("INSERT INTO actualite (act_date,act_title,act_contenu,act_photo,act_contenu_det) VALUES (?,?,?,?,?)");
			ps.setDate(1, datefin);
			ps.setString(2, act_title);
			ps.setString(3, act_contenu);
			ps.setString(4, file);
			ps.setString(5, act_contenu_det);
			ps.executeUpdate();
			
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			try {
				if (ps != null) {
					ps.close();
				}
			} catch (Exception ignore) {
			}
			try {
				if (con != null) {
					con.close();
				}
			} catch (Exception ignore) {
			}
		}
		return true;
	}
	
	public boolean modifierActualite(int idActu, String act_title, String act_contenu, String file, String act_contenu_det) {
		Connection con = null;
		PreparedStatement ps = null;
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		Date date = new Date();
		String dateF = df.format(date.getTime());
		java.sql.Date datefin= java.sql.Date.valueOf(dateF);

		try {
			con = DriverManager.getConnection(URL, UTILISATEUR, MOTDEPASSE);
			ps = con.prepareStatement("UPDATE actualite SET act_date=?,act_title=?,act_contenu=?,act_photo=?,act_contenu_det=? WHERE idActu=?");
			ps.setDate(1, datefin);
			ps.setString(2, act_title);
			ps.setString(3, act_contenu.replace("\n","<br/>"));
			ps.setString(4, file);
			ps.setString(5, act_contenu_det);
			ps.setInt(6, idActu);
			ps.executeUpdate();
			
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			try {
				if (ps != null) {
					ps.close();
				}
			} catch (Exception ignore) {
			}
			try {
				if (con != null) {
					con.close();
				}
			} catch (Exception ignore) {
			}
		}
		return true;
	}
	
	public ArrayList<Actualite> RecupererListeActualite() {

		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList<Actualite> listeActualite = new ArrayList<Actualite>();

		// connexion à la base de données
		try {

			con = DriverManager.getConnection(URL, UTILISATEUR, MOTDEPASSE);
			ps = con.prepareStatement("SELECT * FROM actualite");

			// on exécute la requête
			rs = ps.executeQuery();
			// on parcourt les lignes du résultat
			while (rs.next()) {
				listeActualite.add(
						new Actualite(rs.getInt("idActu"), rs.getDate("act_date"), rs.getString("act_title"), rs.getString("act_contenu"), rs.getString("act_photo"), rs.getString("act_contenu_det")));
			}
			
		} catch (Exception ee) {
			ee.printStackTrace();
		} finally {
			// fermeture du rs, du preparedStatement et de la connexion
			try {
				if (rs != null)
					rs.close();
			} catch (Exception ignore) {
			}
			try {
				if (ps != null)
					ps.close();
			} catch (Exception ignore) {
			}
			try {
				if (con != null)
					con.close();
			} catch (Exception ignore) {
			}
		}
		return listeActualite;

	}
	
	public ArrayList<Actualite> rechercheActualite(String mot_cle) {

		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList<Actualite> listeActualite = new ArrayList<Actualite>();

		// connexion à la base de données
		try {

			con = DriverManager.getConnection(URL, UTILISATEUR, MOTDEPASSE);
			ps = con.prepareStatement("SELECT * FROM actualite WHERE act_title LIKE ? OR act_contenu LIKE ? OR act_contenu_det LIKE ?");
			ps.setString(1, '%'+mot_cle+'%');
			ps.setString(2, '%'+mot_cle+'%');
			ps.setString(3, '%'+mot_cle+'%');
			
			// on exécute la requête
			rs = ps.executeQuery();
			// on parcourt les lignes du résultat
			while (rs.next()) {
				listeActualite.add(
						new Actualite(rs.getInt("idActu"), rs.getDate("act_date"), rs.getString("act_title"), rs.getString("act_contenu"), rs.getString("act_photo"), rs.getString("act_contenu_det")));
			}
			
		} catch (Exception ee) {
			ee.printStackTrace();
		} finally {
			// fermeture du rs, du preparedStatement et de la connexion
			try {
				if (rs != null)
					rs.close();
			} catch (Exception ignore) {
			}
			try {
				if (ps != null)
					ps.close();
			} catch (Exception ignore) {
			}
			try {
				if (con != null)
					con.close();
			} catch (Exception ignore) {
			}
		}
		return listeActualite;

	}
	
	public boolean suppressionActualite(int idActu) {
		Connection con = null;
		PreparedStatement ps = null;

		try {
			con = DriverManager.getConnection(URL, UTILISATEUR, MOTDEPASSE);
			ps = con.prepareStatement("DELETE FROM actualite WHERE idactu=?");
			ps.setInt(1, idActu);
			ps.executeUpdate();
			
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			try {
				if (ps != null) {
					ps.close();
				}
			} catch (Exception ignore) {
			}
			try {
				if (con != null) {
					con.close();
				}
			} catch (Exception ignore) {
			}
		}
		return true;
	}
}